<?php get_header(); ?>

<?php
if (have_posts()) {
	while (have_posts()) {
		the_post();
		?>
		<div class="post">
			<div class="basic-layout">
				<div class="text-container main-container">
					<div class="text">
						<?php
						if (get_post_meta(get_the_ID(), 'Post Top Share Box', true) == 1) :
							include 'includes/share-box.php';
						endif;
						?>
						<?php the_content(); ?>
						<?php
						if (get_post_meta(get_the_ID(), 'Post Bottom Share Box', true) == 1) :
							echo '<div class="clearfix" style="margin-bottom:25px;"></div>';
							include 'includes/share-box.php';
						endif;
						?>
						<div class="clearfix"></div>
						<div class="related-cliparts">
							<?php echo get_related_cliparts_block(); ?>
						</div>
					</div>
					<div class="sidebar">
						<?php echo get_background_single_sidebar(get_the_ID()); ?>
					</div>
				</div>
				<div class="clearfix"></div>
				<?php echo get_basic_get_app_block(); ?>
			</div>

		</div>
		<?php
	}
}
?>


<?php include 'includes/modal-top.php' ?>
<?php include 'includes/modal-bottom.php' ?>

<?php get_footer(); ?>
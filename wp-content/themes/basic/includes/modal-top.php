<div id="top_modal" class="modal fade get-the-app-popup" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="get-the-app-for-free">Get the App for free</h3>
				<div class="content">
          <form id="get_app_form" type="post" class="uk-form c-submit-phone-number" autocomplete="off">
            <label for="phone-number">Send a download link to my phone:</label>
            <input type="text" id="phone-number" name="phone_number" class="pa-input" placeholder="Type your phone number">
            <button data-js-ga-click="get-app get-link source-top-menu" type="submit" class="uk-button c-ga-click pa-btn uk-float-right">Get link</button>
            <span class="js-validation-message validation-message"></span>
          </form>
          <p class="get-the-the-app-label">Download PicsArt app for free</p>
          <div class="uk-clearfix pa-store-buttons">
            <a href="//itunes.apple.com/us/app/picsart-photo-studio/id587366035" class="apple-store c-ga-click" data-js-ga-click="get-app appstore source-top-menu" data-get-ios="bottom-section"></a>
            <a href="//play.google.com/store/apps/details?id=com.picsart.studio" class="google-store c-ga-click" data-js-ga-click="get-app playstore source-top-menu"></a>
            <a href="//www.microsoft.com/en-us/store/p/picsart-photo-studio/9wzdncrfj10m" class="microsoft-store c-ga-click" data-js-ga-click="get-app microsoft source-top-menu"></a>
          </div>
        </div>
            </div>
        </div>
    </div>
</div>
<?php

require_once 'mobile_detect.php';

add_action('admin_menu', 'register_clipart_index');
add_action('admin_enqueue_scripts', 'clipart_shortcode_admin_scripts_init');

function register_clipart_index()
{
	add_menu_page('Cliparts', 'Shortcodes', 'manage_categories', 'cipart', 'clipart_admin_page', 'dashicons-camera', 31);
}

function clipart_shortcode_admin_scripts_init($hook_suffix)
{
	if ($hook_suffix == 'toplevel_page_cipart') {
		wp_enqueue_style('bootstrap', '/wp-content/themes/basic/css/admin/bootstrap.min.css');
	}
}

function clipart_admin_page()
{
	if (!current_user_can('manage_categories')) {
		wp_die(__('You do not have sufficient permissions to access this page.'));
	}
	?>
	<div class="wrap">
		<h1>Shortcodes for Clipart pages</h1>
		<h3>This page displays all available shorcodes.</h3>
		<div class="table-responsive">
			<table class="table table-bordered table-hover gallery_table">
				<thead>
				<tr>
					<th>#</th>
					<th>Shortcode</th>
					<th>Usage</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td> 1.</td>
					<td>[clipart_gallery id="" background_color="" quantity="Write how many cliparts you want to display in a row
						(Now we have ability to show 4, 5, 6, 8 cliparts on one row.)"]
					</td>
					<td> Is used for displaying clipart gallery. You can get the ID from galleries page table.</td>
				</tr>
				<tr>
					<td>2.</td>
					<td>[get_app title="" text="" title_color="" text_color="" background_color=""]</td>
					<td> Is used for displaying get app block.</td>
				</tr>
				<tr>
					<td>3.</td>
					<td>[clipart_content background_color="Fill in background color here"]Place content here[/clipart_content]
					</td>
					<td> Is used for customizing main content.</td>
				</tr>
				<tr>
					<td>4.</td>
					<td>[basic_layout_deeplink text="Place your url text here"]</td>
					<td>Is used for placing deeplinks in basic layout pages</td>
				</tr>
				<tr>
					<td>5.</td>
					<td>[features_single_row]</td>
					<td>Is used for placing rows in features single pages.</td>
				</tr>
				<tr>
					<td>5.</td>
					<td>[share_box]</td>
					<td>Is used for placing share box in the content</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<?php
}

function clipart_app_content($atts, $content = null)
{
	$atts = shortcode_atts(
		array(
			'title' => '',
			'text' => '',
			'title_color' => '#000',
			'text_color' => '#000',
			'background_color' => '#fff'
		), $atts
	);

	$output = '<div class="get_app" style="color: ' . $atts['text_color'] . '; background-color: ' . $atts['background_color'] . ' ">';
	$output .= '<div class="get_app_container main-container">';
	$output .= '<h3 style="color: ' . $atts['title_color'] . '">' . $atts['title'] . '</h3>';
	$output .= '<form id="get_app_form" type="post" class="uk-form c-submit-phone-number" autocomplete="off">';
	$output .= '<label for="phone-number">' . $atts['text'] . '</label>';
	$output .= '<span class="js-validation-message validation-message"></span>';
	$output .= '<input type="text" name="phone_number" id="phone-number" class="pa-input" placeholder="Type your phone number">';
	$output .= '<button data-js-ga-click="clipart get-link source-bottom-section" type="submit" class="uk-button pa-btn c-ga-click">Get link</button>';
	$output .= '</form>';
	$output .= '<div class="uk-clearfix pa-store-buttons">';
	$output .= '<a href="//itunes.apple.com/us/app/picsart-photo-studio/id587366035" class="apple-store c-ga-click" data-js-ga-click="clipart appstore source-bottom-section" ></a>';
	$output .= '<a href="//play.google.com/store/apps/details?id=com.picsart.studio" class="google-store c-ga-click" data-js-ga-click="clipart playstore source-bottom-section" ></a>';
	$output .= '<a href="//apps.microsoft.com/windows/en-us/app/109e8ce6-65e9-4f52-b1b6-3942a9851cd8" class="microsoft-store c-ga-click" data-js-ga-click="clipart microsoft source-bottom-section"></a>';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '</div>';

	return $output;
}

add_shortcode('get_app', 'clipart_app_content');

function clipart_background_content($atts, $content = null)
{
	$atts = shortcode_atts(
		array(
			'background_color' => '#fff'
		), $atts
	);

	$output = '<div class="post_text" style="background-color: ' . $atts['background_color'] . '">';
	$output .= '<div class="main-container">';
	$output .= $content;
	$output .= '</div>';

	return $output;
}

add_shortcode('clipart_content', 'clipart_background_content');


add_filter('the_content', 'remove_empty_p', 20, 1);

function remove_empty_p($content)
{
	$content = force_balance_tags($content);
	return preg_replace('#<span>\s*+(<br\s*/*>)?\s*</span>#i', '', $content);
	return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
}

function add_basic_layout_deeplink($atts)
{
	$atts = shortcode_atts(
		array(
			'text' => ''
		), $atts
	);

	if ($atts['text'] && $atts['text'] != '') {

		if (wp_is_mobile()) {
			$get_app_class = '';
			if (get_post_meta(get_the_ID(), 'Deeplink Url', true)) {
				$deeplink_url = get_post_meta(get_the_ID(), 'Deeplink Url', true);
			} else {
				$deeplink_url = 'picsart://explore';
			}
		} else {
			$get_app_class = 'js-get-app-modal-bottom';
			$deeplink_url = '#';
		}
		$output = '<a data-js-ga-click="clipart get-clipart source-bottom-section" class="c-ga-click ' . $get_app_class . '" href="' . $deeplink_url . '">';
		$output .= $atts['text'];
		$output .= '</a>';

		return $output;
	}
}

add_shortcode('basic_layout_deeplink', 'add_basic_layout_deeplink');

function share_box_shortcode($atts)
{
	$atts = shortcode_atts(
		array(
			'text_color' => '#000'
		), $atts
	);

	$output = '';

	$share_image_url = '';
	if (wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()))) {
		$share_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
	} else if (get_post_meta(get_the_ID(), 'Share Image Url', true)) {
		$share_image_url = get_post_meta(get_the_ID(), 'Share Image Url', true);
	}

	$facebook_share = 'https://www.facebook.com/sharer/sharer.php?u=' . get_the_permalink();
	$twitter_share = 'https://twitter.com/home?status=Check%20out%20this%20article:%20' . get_the_title() . '%20-%20' . get_the_permalink();
	$google_share = 'https://plus.google.com/share?url=' . get_the_permalink();
	$pinterest_share = 'https://pinterest.com/pin/create/button/?url=' . get_the_permalink() . '&media=' . $share_image_url . '&description=' . get_the_title();
	$stumbleUpon_share = 'http://www.stumbleupon.com/submit?url=' . get_the_permalink() . 'title=' . get_the_title();
	$flipboard_share = 'https://share.flipboard.com/bookmarklet/popout?v=2&url=' . get_the_permalink() . 'title=' . get_the_title();
	$reddit_share = 'https://reddit.com/submit?url=' . get_the_permalink() . '&title=' . get_the_title();
	$linkedin_share = 'https://www.linkedin.com/shareArticle?url=' . get_the_permalink() . '&title=' . get_the_title();

	$output .= '<div class="clearfix" style="margin-bottom: 25px;"></div>';
	$output .= '<div class="tags-share-box center-box">';
	$output .= '<div class="post-share">';
	$output .= '<div class="list-posts-share">';
	$output .= '<a target="_blank" href="' . esc_url($facebook_share) . '"><i class="fa fa-facebook fb-icon"></i><span class="dt-share">Facebook</span></a>';
	$output .= '<a target="_blank" href="' . esc_url($twitter_share) . '"><i class="fa fa-twitter twitter-icon"></i><span class="dt-share">Twitter</span></a>';
	$output .= '<a target="_blank" href="' . esc_url($google_share) . '"><i class="fa fa-google-plus google-icon"></i><span class="dt-share">Google +</span></a>';
	$output .= '<a target="_blank" href="' . esc_url($pinterest_share) . '"><i class="fa fa-pinterest pinterest-icon"></i><span class="dt-share">Pinterest</span></a>';
	$output .= '<a target="_blank" href="' . esc_url($stumbleUpon_share) . '"><i class="fa fa-stumbleupon pinterest-icon"></i><span class="dt-share">Stumble</span></a>';
	$output .= '<a target="_blank" href="' . esc_url($flipboard_share) . '"><img src="' . get_template_directory_uri() . '/img/flipboard.png" class="flipboard-icon"/><span class="dt-share">Flipboard</span></a>';
	$output .= '<a target="_blank" href="' . esc_url($reddit_share) . '"><i class="fa fa-reddit reddit-icon"></i><span class="dt-share">Reddit</span></a>';
	$output .= '<a target="_blank" href="' . esc_url($linkedin_share) . '"><i class="fa fa-linkedin linkedin-icon"></i><span class="dt-share">Linkedin</span></a>';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '<div class="clearfix"></div>';

	return $output;
}

add_shortcode('share_box', 'share_box_shortcode');

/* Apps pages shortcodes */

function features_top_image_shortcode($atts, $content = null)
{
	$output = '';
	$atts = shortcode_atts(
		array(
			'learn_more' => '',
			'app' => '',
			'background_image' => ''
		), $atts
	);


	$mobile_detect = new Mobile_Detect();

	if ($mobile_detect->isMobile() && !$mobile_detect->isTablet()) {
		$output .= '<div class="header-title main-container">';
		$output .= '<div class="header-title-items">';
		$output .= $content;
		$output .= '</div>';
		$output .= '<div class="header-buttons">';
		if ($atts['learn_more']) {
			$output .= '<a href="' . $atts['learn_more'] . '" class="learn-now">Learn More</a>';
		}
		if (pa_check_browser($_SERVER['HTTP_USER_AGENT'] == 'web')) {
			$output .= '<button class="get-now c-ga-click js-get-app-modal-apps"
					data-js-ga-click="apps get-app source-bottom-section"
					data-app-urls="' . implode(' ', pa_check_browser_return_app_url($_SERVER['HTTP_USER_AGENT'], $atts['app'], true)) . '">Get Now</button>';
		} else {
			$output .= '<a class="get-now c-ga-click" data-js-ga-click="apps get-app source-bottom-secton"
		 href="' . pa_check_browser_return_app_url($_SERVER['HTTP_USER_AGENT'], $atts['app']) . '"
		 target="_blank">Get Now</a>';
		}
		$output .= '</div>';
		$output .= '</div>';
		$output .= '<div class="products-cover" style="background-image: url(' . $atts['background_image'] . ')"></div>';
	} else {
		$output .= '<div class="position-relative uk-padding-remove products-cover js-products-cover" style="background-image: url(' . $atts['background_image'] . ')">';
		$output .= '<div class="uk-position-cover uk-padding-remove uk-flex uk-flex-center uk-flex-middle">';
		$output .= '<div class="header-title main-container">';
		$output .= '<div class="header-title-items">';
		$output .= $content;
		$output .= '</div>';
		$output .= '<div class="header-buttons">';
		if ($atts['learn_more']) {
			$output .= '<a href="' . $atts['learn_more'] . '" class="learn-now">Learn More</a>';
		}
		if (pa_check_browser($_SERVER['HTTP_USER_AGENT']) == 'web') {
			$output .= '<button class="get-now c-ga-click js-get-app-modal-apps" data-js-ga-click="apps get-app source-bottom-section" data-app-urls="' . implode(' ', pa_check_browser_return_app_url($_SERVER['HTTP_USER_AGENT'], $atts['app'], true)) . '">Get Now</button>';
		} else {
			$output .= '<a class="get-now c-ga-click" data-js-ga-click="apps get-app source-bottom-secton" href="' . pa_check_browser_return_app_url($_SERVER['HTTP_USER_AGENT'], $atts['app']) . '"></a>';
		}
		$output .= '</div>';
		$output .= '</div>';
		$output .= '</div>';
		$output .= '</div>';
	}
	return $output;
}

add_shortcode('features_top_image', 'features_top_image_shortcode');

function product_shortcode($atts, $content = null)
{
	$atts = shortcode_atts(
		array(
			'learn_more' => '',
			'app' => '',
			'logo' => '',
			'strong_title' => '',
			'title' => '',
			'title_style' => ''
		), $atts
	);
	$output = '';

	$mobile_detect = new Mobile_Detect();

	$output .= '<div class="product clearfix">';
	if ($mobile_detect->isMobile() && !$mobile_detect->isTablet()) {
		if ($atts['learn_more'] != '') {
			$output .= '<a href="' . $atts['learn_more'] . '">';
			$output .= '<h2>';
			if ($atts['title_style']) {
				$output .= '<strong style=" display: block;">' . $atts['strong_title'] . '</strong>';
			} else {
				$output .= '<strong>' . $atts['strong_title'] . '</strong>';
			}
			$output .= $atts['title'];
			$output .= '</h2>';
			$output .= '</a>';
		} else {
			$output .= '<h2>';
			$output .= '<strong style="' . $atts['title_style'] ? 'display: block;' : '' . '">' . $atts['strong_title'] . '</strong>';
			$output .= $atts['title'];
			$output .= '</h2>';
		}
	}
	$output .= '<div class="product-content">';
	$output .= '<div class="pa-product-logo">';
	if ($atts['learn_more']) {
		$output .= '<a href="' . $atts['learn_more'] . '">';
		$output .= '<img src="' . $atts['logo'] . '">';
		$output .= '</a>';
	} else {
		$output .= '<img src="' . $atts['logo'] . '">';
	}
	$output .= '<div class="download-section">';
	if ($atts['learn_more']) {
		$output .= '<a href="' . $atts['learn_more'] . '" class="learn-now">Learn More</a>';
	} else {
		$output .= '<div class="margin-placeholder"></div>';
	}

	if (pa_check_browser($_SERVER['HTTP_USER_AGENT']) == 'web') {
		$output .= '<button class="get-now c-ga-click js-get-app-modal-apps" data-js-ga-click="apps get-app source-bottom-section" data-app-urls="' . implode(' ', pa_check_browser_return_app_url($_SERVER['HTTP_USER_AGENT'], $atts['app'], true)) . '">Get Now</button>';
	} else {
		if (pa_check_browser_return_app_url($_SERVER['HTTP_USER_AGENT'], $atts['app']) == '') {
			$output .= '<button class="get-now c-ga-click js-get-app-modal-apps" data-js-ga-click="apps get-app source-bottom-section" data-app-urls="' . implode(' ', pa_check_browser_return_app_url($_SERVER['HTTP_USER_AGENT'], $atts['app'], true)) . '">Get Now</button>';
		} else {
			$output .= '<a class="get-now c-ga-click" data-js-ga-click="apps get-app source-bottom-section" href="' . $atts['learn_more'] . '" target="_blank">Get Now</a>';
		}
	}
	$output .= '</div> <!-- ./download-section -->';
	$output .= '</div> <!-- /.product-logo -->';

	if ($mobile_detect->isMobile() && !$mobile_detect->isTablet()) {
		$output .= '<div class="pa-product-text clearfix">';
	} else {
		$output .= '<div class="pa-product-text">';
	}

	if (!$mobile_detect->isMobile() && !$mobile_detect->isTablet()) {
		if ($atts['learn_more']) {
			$output .= '<a href="' . $atts['learn_more'] . '">';
			$output .= '<h2>';
			$output .= '<strong>' . $atts['strong_title'] . '</strong> ' . $atts['title'];
			$output .= '</h2>';
			$output .= '</a>';
		} else {
			$output .= '<h2>';
			$output .= '<strong>' . $atts['strong_title'] . '</strong> ' . $atts['title'];
			$output .= '</h2>';
		}
	} else {
		if ($mobile_detect->isTablet()) {
			if ($atts['learn_more']) {
				$output .= '<a href="' . $atts['learn_more'] . '">';
				$output .= '<h2>';
				$output .= '<strong>' . $atts['strong_title'] . '</strong> ' . $atts['title'];
				$output .= '</h2>';
				$output .= '</a>';
			} else {
				$output .= '<h2>';
				$output .= '<strong>' . $atts['strong_title'] . '</strong> ' . $atts['title'];
				$output .= '</h2>';
			}
		}
	}
	$output .= $content;
	if ($atts['learn_more']) {
		$output .= '<a class="read-more" href="' . $atts['learn_more'] . '">Read more</a>';
	}

	$output .= '</div> <!-- /.pa-product-text -->';
	$output .= '</div> <!-- /.product-content -->';
	$output .= '</div> <!-- /.product -->';

	return $output;
}

add_shortcode('product', 'product_shortcode');

function product_page_top_section_shortcode($atts, $content = null)
{
	$atts = shortcode_atts(
		array(
			'logo' => '',
			'strong_title' => '',
			'title' => ''
		), $atts
	);
	$output = '';
	$mobile_detect = new Mobile_Detect();
	$output .= '<div class="picsart-photo-studio-info main-container">';
	if ($mobile_detect->isMobile() && !$mobile_detect->isTablet()) {
		$output .= '<div class="text">';
		$output .= '<h2><strong>' . $atts['strong_title'] . '</strong>' . $atts['title'] . '</h2>';
		$output .= '<div class="image">';
		$output .= '<img src="' . $atts['logo'] . '">';
		$output .= '</div>';
		$output .= $content;
		$output .= '</div>';
	} else {
		$output .= '<div class="image">';
		$output .= '<img src="' . $atts['logo'] . '">';
		$output .= '</div>';
		$output .= '<div class="text">';
		$output .= '<h2><strong>' . $atts['strong_title'] . '</strong> ' . $atts['title'] . '</h2>';
		$output .= $content;
		$output .= '</div>';
	}
	$output .= '</div>';

	return $output;
}

add_shortcode('product_page_top_section', 'product_page_top_section_shortcode');

function product_page_video_shortcode($atts)
{
	$atts = shortcode_atts(
		array(
			'url' => ''
		), $atts
	);

	$output = '<div class="picsart-photo-studio-video main-container">';
	$output .= '<iframe src="' . $atts['url'] . '" frameborder="0" width="1920" height="385">';
	$output .= '</div>';

	return $output;
}

add_shortcode('product_page_video', 'product_page_video_shortcode');

function product_page_users_section_shortcode($atts, $content = null)
{
	$atts = shortcode_atts(
		array(
			'title' => '',
			'strong_title' => '',
			'text' => ''
		), $atts
	);

	$output = '<div class="picsart-photo-studio-users clearfix main-container">';
	$output .= '<h2>' . $atts['title'] . ' <strong>' . $atts['strong_title'] . '</strong></h2>';
	$output .= '<p>' . $atts['text'] . '</p>';
	$output .= '<div class="user-items">' . do_shortcode($content) . '</div>';
	$output .= '</div>';

	return $output;
}

add_shortcode('product_page_users_section', 'product_page_users_section_shortcode');

function product_page_user_story_shortcode($atts)
{
	$atts = shortcode_atts(
		array(
			'image' => '',
			'user_image' => '',
			'username' => '',
			'date' => '',
			'text_title' => '',
			'text' => ''
		), $atts
	);
	$output = '';
	$output .= '<div class="user-item">';
	$output .= '<div class="user-image" style="background-image: url(' . $atts['image'] . ')"></div>';
	$output .= '<div class="user-content">';
	$output .= '<div class="user-info">';
	$output .= '<h2>' . $atts['text_title'] . '</h2>';
	$output .= '<p>' . $atts['text'] . '</p>';
	$output .= '</div> <!-- /.user-info -->';
	$output .= '<div class="user-details clearfix">';
	$output .= '<div class="avatar">';
	$output .= '<img src="' . $atts['user_image'] . '">';
	$output .= '</div> <!-- /.avatar -->';
	$output .= '<div class="user-name">';
	$output .= '<h2>' . $atts['username'] . '</h2>';
	$output .= '<span class="date">' . $atts['date'] . '</span>';
	$output .= '</div> <!-- /.user-name -->';
	$output .= '</div> <!-- /.user-details -->';
	$output .= '</div> <!-- /.user-content -->';
	$output .= '</div> <!-- /.user-item -->';

	return $output;
}

add_shortcode('product_page_user_story', 'product_page_user_story_shortcode');

function product_page_middle_image_shortcode($atts)
{
	$atts = shortcode_atts(
		array(
			'background' => ''
		), $atts
	);
	return '<div class="middle-image" style="background-image: url(' . $atts['background'] . ')"></div>';
}

add_shortcode('product_page_middle_image', 'product_page_middle_image_shortcode');

function product_page_simple_text_shortcode($atts, $content = null)
{
	$output = '<div class="picsart-photo-studio-edit-info main-container">';
	$output .= $content;
	$output .= '</div>';

	return $output;
}

add_shortcode('product_page_simple_text', 'product_page_simple_text_shortcode');

function product_page_features_container_shortcode($atts, $content = null)
{
	$atts = shortcode_atts(
		array(
			'title' => '',
			'strong_title' => ''
		), $atts
	);

	$output = '<div class="main-container picsart-photo-studio-features clearfix">';
	$output .= '<h2><strong>' . $atts['strong_title'] . ' </strong>' . $atts['title'] . '</h2>';
	$output .= '<div class="features">' . do_shortcode($content) . '</div>';
	$output .= '</div>';

	return $output;
}

add_shortcode('product_page_features_container', 'product_page_features_container_shortcode');

function product_page_feature_shortcode($atts, $content = null)
{
	$atts = shortcode_atts(
		array(
			'title' => '',
			'background' => '',
			'text' => ''
		), $atts
	);

	$output = '<div class="feature">';
	$output .= '<div class="image" style="background-image: url(' . $atts['background'] . ')">';
	$output .= '<div class="feature-name">';
	$output .= $atts['title'];
	$output .= '</div>';
	$output .= '</div>';
	$output .= '<div class="feature-info">';
	$output .= $atts['text'];
	$output .= '</div>';
	$output .= '</div>';

	return $output;
}

add_shortcode('product_page_feature', 'product_page_feature_shortcode');
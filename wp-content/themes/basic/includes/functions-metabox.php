<?php

//Post main settings metabox
add_action('add_meta_boxes', 'post_main_settings_meta_box_add');

function post_main_settings_meta_box_add()
{
	add_meta_box('post-main-setting-meta-box-id', 'Post Settings', 'post_main_settings_meta_box_cb', ['post', 'collage', 'backgrounds', 'page'], 'normal', 'high');
}

function post_main_settings_meta_box_cb()
{

	$page_title = get_post_meta(get_the_ID(), 'Page Title', true);
	$video_text_color = get_post_meta(get_the_ID(), 'Video Text Color', true);
	$deeplink_url = get_post_meta(get_the_ID(), 'Deeplink Url', true);
	$page_description = get_post_meta(get_the_ID(), 'Page Description', true);
	$page_layout = get_post_meta(get_the_ID(), 'Page Layout', true);
	$post_top_share_box = get_post_meta(get_the_ID(), 'Post Top Share Box', true);
	$post_bottom_share_box = get_post_meta(get_the_ID(), 'Post Bottom Share Box', true);
	$micro_data = get_post_meta(get_the_ID(), 'Micro Data', true);


	wp_nonce_field('my_meta_box_nonce', 'meta_box_nonce');
	$required_field = '<span style="color:red">*</span>';

	?>
	<?php
	if (get_post_type(get_the_ID()) == 'post') :
		?>
		<p>
			<label for="page_layout">Select Page Layout<?php echo $required_field; ?></label>
			<select name="page_layout" id="page_layout">
				<option <?php if (!$page_layout) echo 'selected'; ?> value="3">Not selected
				</option>
				<option <?php if ($page_layout == 1) echo 'selected'; ?> value="1">Basic
				</option>
				<option <?php if ($page_layout == 2) echo 'selected'; ?> value="2">Advanced
				</option>
			</select>
		</p>
		<p>
			<label for="video_text_color">Type Video Text Color</label>
			<input id="video_text_color" type="text" name="video_text_color" value="<?php echo $video_text_color; ?>"/>
		</p>
	<?php endif; ?>
	<?php if (!get_post_type() === 'page'): ?>
	<p>
		<label for="page_top_share_box"> Show Post Top Share Box </label>
		<input type="checkbox" id="page_top_share_box" name="page_top_share_box"
					 value="1" <?php if ($post_top_share_box == 1) echo 'checked'; ?>>
	</p>
	<p>
		<label for="page_bottom_share_box"> Show Post Bottom Share Box </label>
		<input type="checkbox" id="page_bottom_share_box" name="page_bottom_share_box"
					 value="1" <?php if ($post_bottom_share_box == 1) echo 'checked'; ?>>
	</p>
<?php endif; ?>
	<?php if (get_post_type() === 'page'): ?>
	<p>
		<label for="page_title">Type Page Title <?php echo $required_field; ?></label>
		<input id="page_title" type="text" name="page_title" value="<?php echo $page_title; ?>">
	</p>
<?php endif; ?>
	<?php if (!get_post_type() === 'page'): ?>
	<p>
		<label for="deeplink_url">Type the Deeplink Url <?php echo $required_field; ?></label>
		<input id="deeplink_url" type="text" name="deeplink_url" value="<?php echo $deeplink_url; ?>"/>
	</p>
<?php endif; ?>
	<p>
		<label for="page_description">Type the Page Description for Meta <?php echo $required_field; ?></label><br><br>
		<textarea cols="50" rows="4" id="page_description"
							name="page_description"><?php echo $page_description; ?></textarea>
	</p>
	<p>
		<label for="page-microdata">Type the Page Micro Data <?php echo $required_field; ?></label><br><br>
		<textarea cols="50" rows="4" id="page-microdata"
							name="page_microdata"><?php echo $micro_data; ?></textarea>
	</p>

	<?php
}

add_action('save_post', 'post_main_settings_meta_box_save');

function post_main_settings_meta_box_save($post_id)
{
	// Bail if we're doing an auto save
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;

	// if our nonce isn't there, or we can't verify it, bail
	if (!isset($_POST['meta_box_nonce']) || !wp_verify_nonce($_POST['meta_box_nonce'], 'my_meta_box_nonce'))
		return;

	if (wp_is_post_revision($post_id))
		return;

	// Make sure your data is set before trying to save it
	if (isset($_POST['page_title']) && $_POST['page_title'] != '') {
		update_post_meta($post_id, 'Page Title', $_POST['page_title']);
	}
	if (isset($_POST['video_text_color']) && $_POST['video_text_color'] != '') {
		update_post_meta($post_id, 'Video Text Color', $_POST['video_text_color']);
	}
	if (isset($_POST['deeplink_url']) && $_POST['deeplink_url'] != '') {
		update_post_meta($post_id, 'Deeplink Url', $_POST['deeplink_url']);
	}
	if (isset($_POST['page_description']) && $_POST['page_description'] != '') {
		update_post_meta($post_id, 'Page Description', $_POST['page_description']);
	}
	if (isset($_POST['page_layout']) && $_POST['page_layout'] != 3) {
		update_post_meta($post_id, 'Page Layout', $_POST['page_layout']);
	}
	if (isset($_POST['page_microdata']) && $_POST['page_microdata'] != '') {
		update_post_meta($post_id, 'Micro Data', $_POST['page_microdata']);
	}

	if (isset($_POST['page_top_share_box']) && $_POST['page_top_share_box'] != '') {
		update_post_meta($post_id, 'Post Top Share Box', $_POST['page_top_share_box']);
	} else {
		update_post_meta($post_id, 'Post Top Share Box', '0');
	}
	if (isset($_POST['page_bottom_share_box']) && $_POST['page_bottom_share_box'] != '') {
		update_post_meta($post_id, 'Post Bottom Share Box', $_POST['page_bottom_share_box']);
	} else {
		update_post_meta($post_id, 'Post Bottom Share Box', '0');
	}

	//Delete Post Meta Data
	if (isset($_POST['page_title']) && $_POST['page_title'] == '') {
		delete_post_meta($post_id, 'Page Title');
	}
	if (isset($_POST['video_text_color']) && $_POST['video_text_color'] == '') {
		delete_post_meta($post_id, 'Video Text Color');
	}
	if (isset($_POST['deeplink_url']) && $_POST['deeplink_url'] == '') {
		delete_post_meta($post_id, 'Deeplink Url');
	}
	if (isset($_POST['page_description']) && $_POST['page_description'] == '') {
		delete_post_meta($post_id, 'Page Description');
	}
	if (isset($_POST['page_layout']) && $_POST['page_layout'] == 3) {
		delete_post_meta($post_id, 'Page Layout');
	}
	if (isset($_POST['page_microdata']) && $_POST['page_microdata'] == '') {
		delete_post_meta($post_id, 'Micro Data');
	}
}

//Basic layout Get App meta box

add_action('add_meta_boxes', 'post_basic_get_app_meta_box');

function post_basic_get_app_meta_box()
{
	add_meta_box('post-basic-get-app-meta-box-id', 'Post Basic Layout Get App Metabox', 'post_basic_get_app__meta_box_cb', ['post', 'collage', 'backgrounds'], 'normal', 'default');

}

function post_basic_get_app__meta_box_cb()
{

	$app_title = get_post_meta(get_the_ID(), 'Get App Title', true);
	$app_text = get_post_meta(get_the_ID(), 'Get App Text', true);
	$title_color = get_post_meta(get_the_ID(), 'Get App Title Color', true);
	$text_color = get_post_meta(get_the_ID(), 'Get App Text Color', true);
	$background_color = get_post_meta(get_the_ID(), 'Get App Background Color', true);

	wp_nonce_field('my_meta_box_nonce', 'meta_box_nonce');
	?>
	<p>
		<label for="get_app_title">Type The Title</label>
		<input id="get_app_title" type="text" name="get_app_title" value="<?php echo $app_title; ?>"/>
	</p>
	<p>
		<label for="get_app_text">Type The Text </label>
		<input id="get_app_text" type="text" name="get_app_text" value="<?php echo $app_text; ?>"/>
	</p>
	<p>
		<label for="title_color">Type The Title Color</label>
		<input id="title_color" type="text" name="title_color" value="<?php echo $title_color; ?>"/>
	</p>
	<p>
		<label for="text_color">Type The Text Color</label>
		<input id="text_color" type="text" name="text_color" value="<?php echo $text_color; ?>"/>
	</p>
	<p>
		<label for="background_color">Type The Background Color</label>
		<input id="background_color" type="text" name="background_color" value="<?php echo $background_color; ?>"/>
	</p>

	<?php
}

add_action('save_post', 'post_basic_get_app_meta_box_save');

function post_basic_get_app_meta_box_save($post_id)
{
	// Bail if we're doing an auto save
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;

	// if our nonce isn't there, or we can't verify it, bail
	if (!isset($_POST['meta_box_nonce']) || !wp_verify_nonce($_POST['meta_box_nonce'], 'my_meta_box_nonce'))
		return;

	if (wp_is_post_revision($post_id))
		return;

	// Make sure your data is set before trying to save it
	if (isset($_POST['get_app_title']) && $_POST['get_app_title'] != '') {
		update_post_meta($post_id, 'Get App Title', $_POST['get_app_title']);
	}
	if (isset($_POST['get_app_text']) && $_POST['get_app_text'] != '') {
		update_post_meta($post_id, 'Get App Text', $_POST['get_app_text']);
	}
	if (isset($_POST['title_color']) && $_POST['title_color'] != '') {
		update_post_meta($post_id, 'Get App Title Color', $_POST['title_color']);
	}
	if (isset($_POST['text_color']) && $_POST['text_color'] != '') {
		update_post_meta($post_id, 'Get App Text Color', $_POST['text_color']);
	}
	if (isset($_POST['background_color']) && $_POST['background_color'] != 3) {
		update_post_meta($post_id, 'Get App Background Color', $_POST['background_color']);
	}

	//Delete Post Meta Data
	if (isset($_POST['get_app_title']) && $_POST['get_app_title'] == '') {
		delete_post_meta($post_id, 'Get App Title');
	}
	if (isset($_POST['get_app_text']) && $_POST['get_app_text'] == '') {
		delete_post_meta($post_id, 'Get App Text');
	}
	if (isset($_POST['title_color']) && $_POST['title_color'] == '') {
		delete_post_meta($post_id, 'Get App Title Color');
	}
	if (isset($_POST['text_color']) && $_POST['text_color'] == '') {
		delete_post_meta($post_id, 'Get App Text Color');
	}
	if (isset($_POST['background_color']) && $_POST['background_color'] == 3) {
		delete_post_meta($post_id, 'Get App Background Color');
	}
}

//Meta description field for Banner post type

add_action('add_meta_boxes', 'banner_meta_descriptio_meta_box');

function banner_meta_descriptio_meta_box()
{
	add_meta_box('banner-meta-description-meta-box-id', 'Meta Description Metabox', 'banner_meta_description_meta_box_cb', 'banner', 'normal', 'default');
}

function banner_meta_description_meta_box_cb()
{

	$meta_description = get_post_meta(get_the_ID(), 'Banner Meta Description', true);

	wp_nonce_field('my_meta_box_nonce', 'meta_box_nonce');
	?>
	<p>
		<label for="meta_description">Type The Text</label><br/><br/>
		<textarea cols="50" rows="4" id="meta_description"
							name="meta_description"><?php echo $meta_description; ?></textarea>
	</p>

	<?php
}

add_action('save_post', 'banner_meta_description_meta_box_save');

function banner_meta_description_meta_box_save($post_id)
{
	// Bail if we're doing an auto save
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;

	// if our nonce isn't there, or we can't verify it, bail
	if (!isset($_POST['meta_box_nonce']) || !wp_verify_nonce($_POST['meta_box_nonce'], 'my_meta_box_nonce'))
		return;

	if (wp_is_post_revision($post_id))
		return;

	// Make sure your data is set before trying to save it
	if (isset($_POST['meta_description']) && $_POST['meta_description'] != '') {
		update_post_meta($post_id, 'Banner Meta Description', $_POST['meta_description']);
	}

	//Delete Post Meta Data
	if (isset($_POST['meta_description']) && $_POST['meta_description'] == '') {
		delete_post_meta($post_id, 'Banner Meta Description');
	}
}

//Share image url metabox
add_action('add_meta_boxes', 'picsart_share_image_metabox_add');

function picsart_share_image_metabox_add()
{
	add_meta_box('picsart-share-image-metabox', 'Share Image Url', 'picsart_share_image_metabox_cb', ['post', 'collage', 'backgrounds', 'page'], 'side', 'default');
}

function picsart_share_image_metabox_cb()
{

	$image_url = get_post_meta(get_the_ID(), 'Share Image Url', true);

	wp_nonce_field('my_meta_box_nonce', 'meta_box_nonce');
	?>
	<p>
		<label for="share_image_url">Paste the Url</label><br>
		<input style="width: 100%" id="share_image_url" type="text" name="share_image_url"
					 value="<?php echo $image_url; ?>"/>
	</p>

	<?php
}

add_action('save_post', 'picsart_share_image_metabox_save');

function picsart_share_image_metabox_save($post_id)
{
	// Bail if we're doing an auto save
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;

	// if our nonce isn't there, or we can't verify it, bail
	if (!isset($_POST['meta_box_nonce']) || !wp_verify_nonce($_POST['meta_box_nonce'], 'my_meta_box_nonce'))
		return;

	if (wp_is_post_revision($post_id))
		return;

	// Make sure your data is set before trying to save it
	if (isset($_POST['share_image_url']) && $_POST['share_image_url'] != '') {
		update_post_meta($post_id, 'Share Image Url', $_POST['share_image_url']);
	}

	//Delete Post Meta Data
	if (isset($_POST['share_image_url']) && $_POST['share_image_url'] == '') {
		delete_post_meta($post_id, 'Share Image Url');
	}
}
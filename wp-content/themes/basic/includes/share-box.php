<?php
$share_image_url = '';
	if (wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()))) {
		$share_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
	} else if (get_post_meta(get_the_ID(), 'Share Image Url', true)) {
		$share_image_url = get_post_meta(get_the_ID(), 'Share Image Url', true);
	}

$facebook_share  = 'https://www.facebook.com/sharer/sharer.php?u=' . get_the_permalink();
$twitter_share   = 'https://twitter.com/home?status=Check%20out%20this%20article:%20' . get_the_title() . '%20-%20' . get_the_permalink();
$google_share    = 'https://plus.google.com/share?url=' . get_the_permalink();
$pinterest_share = 'https://pinterest.com/pin/create/button/?url=' . get_the_permalink() . '&media=' . $share_image_url . '&description=' . get_the_title();
$stumbleUpon_share = 'http://www.stumbleupon.com/submit?url=' . get_the_permalink() . 'title=' . get_the_title();
$flipboard_share = 'https://share.flipboard.com/bookmarklet/popout?v=2&url=' .get_the_permalink() . 'title=' . get_the_title();
$reddit_share = 'https://reddit.com/submit?url='.get_the_permalink().'&title='.get_the_title();
$linkedin_share = 'https://www.linkedin.com/shareArticle?url='.get_the_permalink().'&title='.get_the_title();

	$post_type = (get_post_type() == 'post') ? 'clipart' : get_post_type();
?>

<div class="tags-share-box center-box">
	<div class="post-share">
		<div class="list-posts-share">
			<a class="c-ga-click" data-js-ga-click="<?php echo $post_type; ?> share facebook" target="_blank" href="<?php echo esc_url( $facebook_share ); ?>"><i class="fa fa-facebook fb-icon"></i><span class="dt-share"><?php esc_html_e( 'Facebook', 'basic' ); ?></span></a>
			<a class="c-ga-click" data-js-ga-click="<?php echo $post_type; ?> share twitter" target="_blank" href="<?php echo esc_url( $twitter_share ); ?>"><i class="fa fa-twitter twitter-icon"></i><span class="dt-share"><?php esc_html_e( 'Twitter', 'basic' ); ?></span></a>
			<a class="c-ga-click" data-js-ga-click="<?php echo $post_type; ?> share google" target="_blank" href="<?php echo esc_url( $google_share ); ?>"><i class="fa fa-google-plus google-icon"></i><span class="dt-share"><?php esc_html_e( 'Google +', 'basic' ); ?></span></a>
			<a class="c-ga-click" data-js-ga-click="<?php echo $post_type; ?> share pinterest" target="_blank" href="<?php echo esc_url( $pinterest_share ); ?>"><i class="fa fa-pinterest pinterest-icon"></i><span class="dt-share"><?php esc_html_e( 'Pinterest', 'basic' ); ?></span></a>
			<a class="c-ga-click" data-js-ga-click="<?php echo $post_type; ?> share stumbleupon" target="_blank" href="<?php echo esc_url( $stumbleUpon_share ); ?>"><i class="fa fa-stumbleupon stumble-icon"></i><span class="dt-share">Stumble</span></a>
			<a class="c-ga-click" data-js-ga-click="<?php echo $post_type; ?> share flipboard" target="_blank" href="<?php echo esc_url( $flipboard_share ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/flipboard.png" class="flipboard-icon"/><span class="dt-share">Flipboard</span></a>
			<a class="c-ga-click" data-js-ga-click="<?php echo $post_type; ?> share reddit" target="_blank" href="<?php echo esc_url( $reddit_share ); ?>"><i class="fa fa-reddit reddit-icon"></i><span class="dt-share"><?php esc_html_e( 'Reddit', 'basic' ); ?></span></a>
			<a class="c-ga-click" data-js-ga-click="<?php echo $post_type; ?> share linkedin" target="_blank" href="<?php echo esc_url( $linkedin_share ); ?>"><i class="fa fa-linkedin linkedin-icon"></i><span class="dt-share"><?php esc_html_e( 'Linkedin', 'basic' ); ?></span></a>
		</div>
	</div>
</div>
<?php
require_once 'mobile_detect.php';
/* ==========================================================================
 *  General Functions for all pages
 * ========================================================================== */
/* SEO functions */
function pa_render_meta_title()
{
	if (is_404()) {
		echo 'Error';
	} elseif (is_post_type_archive('collage')) {
		echo 'Collage by PicsArt';
	} elseif (is_singular('collage')) {
		wp_title('- Collage by PicsArt', true, 'right');
	} elseif (is_post_type_archive('backgrounds')) {
		echo 'Backgrounds by Picsart';
	} elseif (is_singular('backgrounds')) {
		wp_title('- Backgrounds by PicsArt', true, 'right');
	} elseif (get_post_type() == 'page') {
		echo get_post_meta(get_the_ID(), 'Page Title', true);
	} else {
		wp_title('-', true, 'right');
		bloginfo('name');
	}
}

function pa_check_browser_return_app_url($user_agent, $app_type, $suggestions = false)
{
	$app_links = array(
		'picsart' => array(
			'android' => 'https://play.google.com/store/apps/details?id=com.picsart.studio&hl=en',
			'apple' => 'https://itunes.apple.com/am/app/picsart-photo-studio-collage-maker-pic-editor/id587366035?mt=8',
			'windows' => 'https://www.microsoft.com/en-am/store/p/picsart-photo-studio/9wzdncrfj10m'
		),
		'animator' => array(
			'android' => 'https://play.google.com/store/apps/details?id=com.picsart.animate',
			'apple' => 'https://itunes.apple.com/am/app/picsart-animated-gif-video-animator/id1181847209?mt=8'
		),
		'color_paint' => array(
			'android' => 'https://play.google.com/store/apps/details?id=com.picsart.draw',
			'apple' => 'https://itunes.apple.com/am/app/picsart-color-paint/id1183817887?mt=8'
		),
		'magic_video' => array(
			'apple' => 'https://itunes.apple.com/am/app/picsart-video-maker-movie-editor/id1155605927?mt=8',
		)
	);
	$browser = pa_check_browser($user_agent);
	if ($suggestions) {
		return $app_links[$app_type];
	} else {
		if ($browser != 'web') {
			return $app_links[$app_type][$browser];
		} else {
			return 'web';
		}
	}
}

function pa_check_browser($user_agent)
{
	$user_agent = strtolower($user_agent);
	preg_match('/android|iphone|ipod|ipad|windows phone/', $user_agent, $matches);
	$matches = $matches[0];
	if ($matches == 'android') {
		return 'android';
	} elseif ($matches == 'iphone' || $matches == 'ipod' || $matches == 'ipad') {
		return 'apple';
	} elseif ($matches == 'windows phone') {
		return 'windows';
	} else {
		return 'web';
	}
}

function pa_render_meta_deeplink()
{
	if (get_post_meta(get_the_ID(), 'Deeplink Url', true)) {
		$deeplink_url = get_post_meta(get_the_ID(), 'Deeplink Url', true);
		if (is_post_type_archive('collage')) {
			$deeplink_url = 'picsart://shop?category=collage_frames';
		} elseif (is_post_type_archive('backgrounds')) {
			$deeplink_url = 'picsar://backgrounds';
		}
	} elseif (is_post_type_archive('collage') || is_singular('collage')) {
		$deeplink_url = 'picsart://shop?category=collage_frames';
	} elseif (is_post_type_archive('backgrounds') || is_singular('backgrounds')) {
		$deeplink_url = 'picsart://shop?category=backgrounds';
	} else {
		$deeplink_url = 'picsart://clipart';
	}
	return $deeplink_url;
}

function pa_render_og_title()
{
	if (is_front_page() || is_page('clipart')) {
		$og_title = 'Clipart Collections - PicsArt';
	} elseif (is_post_type_archive('apps')) {
		$og_title = 'See More Apps By PicsArt';
	} elseif (is_tax('picsart_apps_subtype')) {
		$og_title = 'Mobile Photo Editor and Collage Maker - PicsArt';
	} else {
		$og_title = wp_title('- PicsArt', false, 'right');
	}
	return $og_title;
}

function pa_render_share_image_url()
{
	$image_url = get_post_meta(get_the_ID(), 'Share Image Url', true);
	if ($image_url && $image_url != '') {
		if (is_post_type_archive('collage')) {
			$image_url = get_site_url() . '/wp-content/themes/basic/img/collage-banner.jpg';
		}
	} else if (is_page('clipart')) {
		$image_url = get_site_url() . '/wp-content/themes/basic/img/clipart-banner.jpg';
	} else if (is_post_type_archive('backgrounds')) {
		$image_url = get_site_url() . '/wp-content/themes/basic/img/backgrounds-banner.jpg';
	} else {
		$image_url = get_site_url() . '/wp-content/themes/basic/img/og-image-jobs.jpg';
	}
	return $image_url;
}

function pa_render_meta_description()
{
	$page_description = '';
	if (get_post_meta(get_the_ID(), 'Page Description', true)) {
		$page_description = get_post_meta(get_the_ID(), 'Page Description', true);
	} elseif (is_front_page() || is_page('clipart') || is_singular('post')) {
		$page_description = 'Free Clip Art to use on images, memes and collages.';
	} elseif (is_post_type_archive('collage') || is_singular('collage')) {
		$page_description = 'Use PicsArt\'s free collage maker with hundreds of layouts, frames, backgrounds, and stickers to make photo collages.';
	} elseif (is_post_type_archive('backgrounds') || is_singular('backgrounds')) {
		$page_description = 'Find the perfect background for your photos and collages with PicsArt. Choose from hundreds of cool backgrounds to use as a wallpaper or graphic background';
	}
	return $page_description;
}

/* End of SEO functions */

//Get Post Attachment
function custom_get_post_attachment($post_id)
{
	$args = array(
		'post_type' => 'attachment',
		'posts_per_page' => 1,
		'post_parent' => $post_id,
		'numberposts' => 1,
		'exclude' => get_post_thumbnail_id()
	);
	$attachments = get_posts($args);
	$attachment_url = '';
	foreach ($attachments as $attachment) {
		$attachment_url = $attachment->guid;
	}
	return $attachment_url;
}

//Get post excrept with character limits
function custom_get_excerpt($post_id, $limit)
{
	if ($limit == null) {
		$excerpt = get_the_excerpt($post_id);
	} else {
		$excerpt = explode(' ', get_the_excerpt($post_id), $limit);
		if (count($excerpt) >= $limit) {
			array_pop($excerpt);
			$excerpt = implode(" ", $excerpt) . '...';
		} else {
			$excerpt = implode(" ", $excerpt);
		}
		$excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
	}
	return $excerpt;
}

//Function for showing cliparts list in header menu
function header_clipart_list()
{
	$args = array('offset' => 0,
		'posts_per_page' => 15,
		'orderby' => 'date',
		'category' => 3,
		'order' => 'DESC',
		'post_type' => 'post',
		'post_status' => 'publish',
		'suppress_filters' => true,
	);
	$posts_array = get_posts($args);
	$output = '';
	if ($posts_array && $posts_array != '') {
		if (wp_is_mobile()) {
			$output = '<div class="uk-list">';
			$output .= '<ul class="uk-list-container">';
		} else {
			$output = '<ul id="uk-list-container" class="uk-list-container">';
		}
		foreach ($posts_array as $post) {
			if ($post->ID == get_the_ID() && !is_front_page()) {
				$output .= '<li class="list-item current">';
			} else {
				$output .= '<li class="list-item">';
			}
			$output .= '<a href="' . get_permalink($post->ID) . '">';
			$output .= get_the_title($post->ID);
			$output .= '</a>';
			$output .= '</li>';
		}
		$output .= '</ul>';
		if (wp_is_mobile()) {
			$output .= '</div>';
		}
	}
	return $output;
}

//Random logo generator
function random_logo_generator()
{
	$number = rand(1, 8);
	$logo = '<img src="https://picsart.com/images/website-redesign/wordmark/' . $number . '.png" alt="PicsArt Mobile Photo Editor" title="PicsArt Mobile Photo Editor" class="c-random-wordmark visible" />';
	return $logo;
}

//Get front page top block
function get_featured_block($post_type)
{
	$args = array(
		'offset' => 0,
		'posts_per_page' => 1,
		'orderby' => 'date',
		'order' => 'DESC',
		'post_status' => 'publish',
		'suppress_filters' => true,
	);
	if ($post_type == 'post') {
		$args['post_type'] = 'post';
		$args['category'] = 2;
	} else if ($post_type == 'collage') {
		$args['post_type'] = 'collage';
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'picsart_collage_subtype',
				'field' => 'slug',
				'terms' => 'collage-featured'
			)
		);
	} else if ($post_type == 'backgrounds') {
		$args['post_type'] = 'backgrounds';
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'picsart_backgrounds_subtype',
				'field' => 'slug',
				'terms' => 'backgrounds-featured'
			)
		);
	}
	$posts_array = get_posts($args);
	$output = '';
	if ($posts_array && $posts_array != '') {
		foreach ($posts_array as $post_item) {
			if ($post_type == 'collage' || $post_type == 'backgrounds') {
				$output = '<div class="main-container">';
			}
			$output .= '<div class="header-section" style="background: url(' . get_the_post_thumbnail_url($post_item->ID) . ') center no-repeat;">';
			if ($post_type == 'post') {
				$output .= '<div class="uk-position-cover uk-padding-remove uk-flex uk-flex-center uk-flex-middle">';
				$output .= '<div class="header-title main-container">';
				$output .= '<h1>' . $post_item->post_title . '</h1>';
				$output .= '</div>';
				$output .= '</div>';
			}
			$output .= '</div>';
			if ($post_type == 'collage' || $post_type == 'backgrounds') {
				$output .= '</div>';
			}
		}
	}
	return $output;
}

//Basic layout get app block
function get_basic_get_app_block()
{
	$app_title = get_post_meta(get_the_ID(), 'Get App Title', true);
	$app_text = get_post_meta(get_the_ID(), 'Get App Text', true);
	$title_color = get_post_meta(get_the_ID(), 'Get App Title Color', true);
	$text_color = get_post_meta(get_the_ID(), 'Get App Text Color', true);
	$background_color = get_post_meta(get_the_ID(), 'Get App Background Color', true);
	$output = '';
	if ($app_title && $app_title != '') {
		$output = '<div class="get_app basic-get-app" style="color: ' . $text_color . '; background-color: ' . $background_color . '">';
		$output .= '<div class="get_app_container main-container">';
		$output .= '<h3 style="color: ' . $title_color . '">' . $app_title . '</h3>';
		$output .= '<form id="get_app_form" type="post" class="uk-form c-submit-phone-number" autocomplete="off">';
		$output .= '<label for="phone-number">' . $app_text . '</label>';
		$output .= '<span class="js-validation-message validation-message"></span>';
		$output .= '<input type="text" name="phone_number" id="phone-number" class="pa-input" placeholder="Type your phone number">';
		$output .= '<button data-js-ga-click="get-app get-link source-bottom-section" type="submit" class="uk-button pa-btn c-ga-click">Get link</button>';
		$output .= '</form>';
		$output .= '<div class="uk-clearfix pa-store-buttons">';
		$output .= '<a href="//itunes.apple.com/us/app/picsart-photo-studio/id587366035" class="apple-store c-ga-click" data-js-ga-click="get-app appstore source-bottom-section"></a>';
		$output .= '<a href="//play.google.com/store/apps/details?id=com.picsart.studio" class="google-store c-ga-click" data-js-ga-click="clipart playstore source-bottom-section"></a>';
		$output .= '<a href="//www.microsoft.com/en-us/store/p/picsart-photo-studio/9wzdncrfj10m" class="microsoft-store c-ga-click" data-js-ga-click="clipart microsoft source-bottom-section"></a>';
		$output .= '</div>';
		$output .= '</div>';
		$output .= '</div>';
	}
	return $output;
}

function pa_get_search_results($query, $name)
{
	if ($query->have_posts()) {
		?>
		<div class="results-container">
			<?php
			if ($query->post_count > 1) {
				$name = $name . 's';
			}
			?>
			<h1
				class="search-query"><?php echo $query->post_count . __(' ' . $name . ' found for: ', 'basic') . get_query_var('q'); ?></h1>
			<ul class="results">
				<?php
				while ($query->have_posts()) {
					$query->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a>
						<p><?php echo custom_get_excerpt(get_the_ID(), null) ?></p>
					</li>
					<?php
				}
				?>
			</ul>
		</div> <!-- .results-container -->
		<?php
	}
	wp_reset_postdata();
}

/* ==========================================================================
 *  Clipart pages functions section
 * ========================================================================== */
//Function for getting posts used in front page category widgets
function get_front_posts($cat_id)
{
	$args = array(
		'offset' => 0,
		'category' => $cat_id,
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'DESC',
		'post_type' => 'post',
		'post_status' => 'publish',
		'suppress_filters' => true
	);
	$posts_array = get_posts($args);
	$output = '';
	if ($posts_array && $posts_array != '') {
		$cat_id = (int)$cat_id;
		$category = get_category($cat_id);
		$output = '<li id="' . $category->slug . '">';
		$output .= '<h3 class="clipart-cat-title">' . $category->name . '</h3>';
		if (count($posts_array) == 1) {
			foreach ($posts_array as $post_item) {
				$output .= '<a href="' . get_permalink($post_item->ID) . '" target="_blank">' . $post_item->post_title . '</a>';
				$output .= '<p>' . custom_get_excerpt($post_item->ID, null) . '</p>';
			}
		} else {
			$output .= '<ul class="clipart-list">';
			foreach ($posts_array as $post_item) {
				$output .= '<li>';
				$output .= '<a href="' . get_permalink($post_item->ID) . '" target="_blank">' . $post_item->post_title . '</a>';
				$output .= '<p>' . custom_get_excerpt($post_item->ID, null) . '</p>';
				$output .= '</li>';
			}
			$output .= '</ul>';
		}
		$output .= '</li>';
	}
	return $output;
}

//Basic layout sidebar
function get_clipart_sidebar($id)
{
	$cat_ids = get_the_category($id);
	$current_cat_id = '';
	foreach ($cat_ids as $cat_id) {
		if ($cat_id->term_id == 3 || $cat_id->term_id == 2 || $cat_id->slug == 'uncategorized') {
			continue;
		} else {
			$current_cat_id = $cat_id->term_id;
			break;
		}
	}
	$args = array(
		'offset' => 0,
		'category' => $current_cat_id,
		'posts_per_page' => 10,
		'orderby' => 'date',
		'order' => 'DESC',
		'post_type' => 'post',
		'post_status' => 'publish',
		'suppress_filters' => true,
		'exclude' => get_the_ID()
	);
	$posts_array = get_posts($args);
	$output = '';
	if ($posts_array && $posts_array != '') {
		$output = '<h4 class="sidbar-title">Similar Packages</h4>';
		$output .= '<ul class="sidebar-items">';
		foreach ($posts_array as $post_item) {
			$output .= '<li class="item">';
			$output .= '<div class="item-text">';
			$output .= '<a href="' . get_permalink($post_item->ID) . '">' . $post_item->post_title . '</a>';
			$output .= '</div>';
			$output .= '<div class="item-desc">';
			$output .= custom_get_excerpt($post_item->ID, 7);
			$output .= '</div>';
			$output .= '</li>';
		}
		$output .= '</ul>';
	}
	return $output;
}

//Front page sidebar generator
function get_frontpage_sidebar()
{
	$args = array(
		'offset' => 0,
		'category' => 3,
		'posts_per_page' => 10,
		'orderby' => 'date',
		'order' => 'DESC',
		'post_type' => 'post',
		'post_status' => 'publish',
		'suppress_filters' => true,
		'exclude' => get_the_ID()
	);
	$posts_array = get_posts($args);
	$output = '';
	if ($posts_array && $posts_array != '') {
		$output = '<h4 class="sidbar-title">Trending Cliparts</h4>';
		$output .= '<ul class="sidebar-items">';
		foreach ($posts_array as $post_item) {
			$output .= '<li class="item">';
			$output .= '<div class="item-text">';
			$output .= '<a href="' . get_permalink($post_item->ID) . '">' . $post_item->post_title . '</a>';
			$output .= '</div>';
			$output .= '<div class="item-desc">';
			$output .= custom_get_excerpt($post_item->ID, 7);
			$output .= '</div>';
			$output .= '</li>';
		}
		$output .= '</ul>';
	}
	return $output;
}

/* ==========================================================================
 *  Functions for Collage pages
 * ========================================================================== */
//Function for getting front page frames block
function get_front_frames_block()
{
	$args = array(
		'offset' => 0,
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'DESC',
		'post_type' => 'collage',
		'post_status' => 'publish',
		'supress_filter' => true,
		'tax_query' => array(
			array(
				'taxonomy' => 'picsart_collage_subtype',
				'field' => 'slug',
				'terms' => 'frames'
			)
		)
	);
	$posts_array = get_posts($args);
	$output = '';
	if ($posts_array && $posts_array != '') {
		$output .= '<h2 class="frame-block-heading">Frames</h2>';
		foreach ($posts_array as $post) {
			$output .= '<div class="item">';
			$output .= '<div class="item-image">';
			$output .= get_the_post_thumbnail($post->ID);
			$output .= '</div>';
			$output .= '<div class="item-title">';
			$output .= '<a href="' . get_permalink($post->ID) . '">' . $post->post_title . '</a>';
			$output .= '</div>';
			$output .= '<div class="item-text">';
			$output .= custom_get_excerpt($post->ID, 21);
			$output .= '</div>';
			$output .= '</div>';
		}
		$output .= '<div class="clearfix"></div>';
	}
	return $output;
}

//Function for getting main page and single page related clipart block
function get_related_cliparts_block()
{
	$args = array(
		'offset' => 0,
		'posts_per_page' => 10,
		'orderby' => 'date',
		'category' => 3,
		'order' => 'DESC',
		'post_type' => 'post',
		'post_status' => 'publish',
		'suppress_filters' => true,
	);
	$posts_array = get_posts($args);
	$output = '';
	if ($posts_array && $posts_array != '') {
		$output = '<h4 class="clipart-cat-title">Related Cliparts</h4>';
		$output .= '<ul class="clipart-list">';
		foreach ($posts_array as $post_item) {
			$output .= '<li class="item">';
			$output .= '<a href="' . get_permalink($post_item->ID) . '">' . $post_item->post_title . '</a>';
			$output .= '<p>' . custom_get_excerpt($post_item->ID, null) . '</p>';
			$output .= '</li>';
		}
		$output .= '</ul>';
	}
	return $output;
}

//Function for get Collage Front page sidebar
function get_collage_front_sidebar()
{
	$args = array(
		'offset' => 0,
		'posts_per_page' => 10,
		'orderby' => 'date',
		'category' => 3,
		'order' => 'DESC',
		'post_type' => 'post',
		'post_status' => 'publish',
		'suppress_filters' => true,
	);
	$posts_array = get_posts($args);
	$output = '';
	if ($posts_array && $posts_array != '') {
		$output = '<h4 class="sidbar-title">Related Cliparts</h4>';
		$output .= '<ul class="sidebar-items">';
		foreach ($posts_array as $post_item) {
			$output .= '<li class="item">';
			$output .= '<div class="item-text">';
			$output .= '<a href="' . get_permalink($post_item->ID) . '">' . $post_item->post_title . '</a>';
			$output .= '</div>';
			$output .= '<div class="item-desc">';
			$output .= custom_get_excerpt($post_item->ID, 7);
			$output .= '</div>';
			$output .= '</li>';
		}
		$output .= '</ul>';
	}
	return $output;
}

//Function for getting Collage single page sidebar
function get_collage_single_sidebar($id, $term)
{
	$args = array(
		'offset' => 0,
		'posts_per_page' => 4,
		'orderby' => 'date',
		'order' => 'DESC',
		'post_type' => 'collage',
		'post_status' => 'publish',
		'suppress_filters' => true,
		'exclude' => $id,
		'tax_query' => array(
			array(
				'taxonomy' => 'picsart_collage_subtype',
				'field' => 'slug',
				'terms' => $term
			)
		)
	);
	$posts_array = get_posts($args);
	$output = '';
	if ($posts_array && $posts_array != '') {
		$output = '<h4 class="sidbar-title">Similar Frames</h4>';
		$output .= '<ul class="sidebar-items">';
		foreach ($posts_array as $post_item) {
			$output .= '<li class="item">';
			$output .= '<div class="item-text">';
			$output .= '<a href="' . get_permalink($post_item->ID) . '">' . $post_item->post_title . '</a>';
			$output .= '</div>';
			$output .= '<div class="item-desc">';
			$output .= custom_get_excerpt($post_item->ID, 7);
			$output .= '</div>';
			$output .= '</li>';
		}
		$output .= '</ul>';
	}
	return $output;
}

/* ==========================================================================
 *  Functions for Backgrounds pages
 * ========================================================================== */
//Function for getting Background front posts
function get_front_backgrounds_block()
{
	$args = array(
		'offset' => 0,
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'DESC',
		'post_type' => 'backgrounds',
		'post_status' => 'publish',
		'supress_filter' => true,
		'tax_query' => array(
			array(
				'taxonomy' => 'picsart_backgrounds_subtype',
				'field' => 'slug',
				'terms' => 'backgrounds-front'
			)
		)
	);
	$posts_array = get_posts($args);
	$output = '';
	if ($posts_array && $posts_array != '') {
		$output .= '<h2 class="frame-block-heading">Backgrounds</h2>';
		foreach ($posts_array as $post) {
			$output .= '<div class="item">';
			$output .= '<div class="item-image">';
			$output .= get_the_post_thumbnail($post->ID);
			$output .= '</div>';
			$output .= '<div class="item-title">';
			$output .= '<a href="' . get_permalink($post->ID) . '">' . $post->post_title . '</a>';
			$output .= '</div>';
			$output .= '<div class="item-text">';
			$output .= custom_get_excerpt($post->ID, 21);
			$output .= '</div>';
			$output .= '</div>';
		}
		$output .= '<div class="clearfix"></div>';
	}
	return $output;
}

//Function for getting Backgrounds single page sidebar
function get_background_single_sidebar($id)
{
	$args = array(
		'offset' => 0,
		'posts_per_page' => 4,
		'orderby' => 'date',
		'order' => 'DESC',
		'post_type' => 'backgrounds',
		'post_status' => 'publish',
		'suppress_filters' => true,
		'exclude' => $id,
		'tax_query' => array(
			array(
				'taxonomy' => 'picsart_backgrounds_subtype',
				'field' => 'slug',
				'terms' => 'backgrounds-front'
			)
		)
	);
	$posts_array = get_posts($args);
	$output = '';
	if ($posts_array && $posts_array != '') {
		$output = '<h4 class="sidbar-title">Similar Backgrounds</h4>';
		$output .= '<ul class="sidebar-items">';
		foreach ($posts_array as $post_item) {
			$output .= '<li class="item">';
			$output .= '<div class="item-text">';
			$output .= '<a href="' . get_permalink($post_item->ID) . '">' . $post_item->post_title . '</a>';
			$output .= '</div>';
			$output .= '<div class="item-desc">';
			$output .= custom_get_excerpt($post_item->ID, 7);
			$output .= '</div>';
			$output .= '</li>';
		}
		$output .= '</ul>';
	}
	return $output;
}

/* ==========================================================================
 *  Functions for Apps pages
 * ========================================================================== */
function pa_apps_get_features($id)
{
	$new_wp_query = new WP_Query();
	$all_child_pages = $new_wp_query->query(
		array(
			'post_type' => 'page',
			'posts_pet_page' => '-1',
			'post_status' => 'publish',
			'orderby' => 'date',
			'order' => 'DESC'));
	$children = get_page_children($id, $all_child_pages);
	$output = '';
	if (count($children)) {
		$output = '<div class="main-container picsart-photo-studio-features clearfix">';
		$output .= '<h2><strong>Create</strong> Pictures, Collages and Drawings</h2>';
		$output .= '<div class="features">';
		foreach ($children as $child) {
			$output .= '<div class="feature">';
			$output .= '<a href="' . get_the_permalink($child->ID) . '">';
			$output .= '<div class="image" style="background-image: url(' . get_the_post_thumbnail_url($child->ID) . ')">';
			$output .= '<div class="feature-name">';
			$output .= $child->post_title;
			$output .= '</div>';
			$output .= '</div>';
			$output .= '</a>';
			$output .= '<div class="feature-info">';
			$output .= '<p>' . $child->post_excerpt . '</p>';
			$output .= '</div>';
			$output .= '</div>';
		}
		$output .= '</div>';
		$output .= '</div>';
	}
	return $output;
}

function pa_apps_get_now_section($app_name)
{
	$output = '<div class="get-now-section position-relative">';
	$output .= '<div class="get-now-section-content">';
	$output .= '<h2>Did you Makeup your Mind?</h2>';
	if (pa_check_browser($_SERVER['HTTP_USER_AGENT']) == 'web') {
		$output .= '<button class="get-now c-ga-click js-get-app-modal-apps" data-js-ga-click="apps get-app source-bottom-section" data-app-urls="' . implode(' ', pa_check_browser_return_app_url($_SERVER['HTTP_USER_AGENT'], $app_name, true)) . '">Get Now</button>';
	} else {
		$output .= '<a class="get-now c-ga-click" data-js-ga-click="apps get-app source-bottom-secton" href="' . pa_check_browser_return_app_url($_SERVER['HTTP_USER_AGENT'], $app_name) . '" target="_blank">Get Now</a>';
	}
	$output .= '</div>';
	$output .= '</div>';
	return $output;
}

function pa_get_suggested_apps($id)
{
	$picsart_apps = new WP_Query();
	$apps = get_page_by_title('Apps');
	$all_picsart_apps = $picsart_apps->query(
		array(
			'post_type' => 'page',
			'posts_per_page' => '-1',
			'post_status' => 'publish',
			'post_parent' => $apps->ID,
			'post__not_in' => array($id)
		)
	);
	$mobile_detect = new Mobile_Detect();
	$apps_children = get_page_children($apps->ID, $all_picsart_apps);
	$output = '';
	if (count($apps_children)) {
		$output = '<div class="main-container suggested-apps clearfix">';
		$output .= '<h2>See also</h2>';
		foreach ($apps_children as $apps_child) {
			$output .= '<div class="app">';
			$output .= '<a href="' . get_the_permalink($apps_child->ID) . '">';
			$output .= '<img src="' . get_the_post_thumbnail_url($apps_child->ID) . '">';
			$output .= '<h3>' . $apps_child->post_title . '</h3>';
			if ($mobile_detect->isMobile() && !$mobile_detect->isTablet()) {
				$output .= '<img class="gradient_border" src="' . get_template_directory_uri() . '/img/apps/gradient_border.png' . '">';
			}
			$output .= '</a>';
			$output .= '</div>';
		}
		$output .= '</div>';
	}
	return $output;
}

function pa_get_apps_pagination($id)
{
	$page_parent = wp_get_post_parent_id($id);
	$new_query = new WP_Query();
	$all_child_pages = $new_query->query(
		array(
			'post_type' => 'page',
			'posts_pet_page' => '2',
			'post_status' => 'publish',
			'orderby' => 'date',
			'order' => 'DESC',
			'post__not_in' => array($id)
		));
	$children = get_page_children($page_parent, $all_child_pages);
	$output = '';
	if ($children) {
		$output = '<div class="pagination main-container clearfix">';
		$output .= '<a class="back-button" href="' . get_the_permalink($children[0]->ID) . '">< ' . $children[0]->post_title . '</a>';
		$output .= '<a class="front-button" href="' . get_the_permalink($children[1]->ID) . '">' . $children[1]->post_title . ' ></a>';
		$output .= '</div>';
	}
	return $output;
}
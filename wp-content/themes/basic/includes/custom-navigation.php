<?php

global $post;

/* Get an array of Ancestors and Parents if they exist */
$parents = get_post_ancestors($post->ID);
/* Get the top Level page->ID count base 1, array base 0 so -1 */
$id = ($parents) ? $parents[count($parents) - 1] : $post->ID;
/* Get the parent and set the $class with the page slug (post_name) */
$parent = get_post($id);
$class = $parent->post_name;
?>

<nav <?php if (is_404()) {
	echo 'style="display:none!important;"';
} ?> id="top-nav" class="<?php if (!wp_is_mobile()) {
	echo 'nav-with-submenu';
} else {
	echo 'js-top-nav js-main-navigation pa-mobile to-top';
} ?> ">
	<div class="pa-standard-container main-container clearfix">
		<?php
		if (wp_is_mobile()) {
			?>
			<a href="/explore" class="c-active float-left navigation-logo" data-js-active="/explore">
				<svg width="22px" height="22px" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg"
						 xmlns:xlink="http://www.w3.org/1999/xlink">
					<g id="page-4" stroke="none" stroke-width="0" fill="none" fill-rule="evenodd">
						<g id="home-icon-svg" transform="translate(-16.000000, -83.000000)" fill-rule="nonzero" fill="#888888">
							<g id="mobile-Top-Navigation" transform="translate(0.000000, 64.000000)">
								<path
									d="M19.1932852,28.0781115 L19.1932852,28.5779525 C19.3573561,28.5779525 19.4903292,28.5189089 19.5843564,28.4152613 L27.3664001,19.8369908 L26.6335999,19.8369908 L34.4156436,28.4152613 C34.5096708,28.5189089 34.6426439,28.5779525 34.7820437,28.5779525 L34.8069134,28.0781115 L34.3106124,28.0781115 C34.3106124,28.2302988 34.3569481,28.3505604 34.4405133,28.4426755 L37.1363208,31.4143071 L37.5027209,30.5773163 L34.8069134,30.5773163 C34.5328139,30.5773163 34.3106124,30.8011027 34.3106124,31.0771573 L34.3106124,40.500159 L34.8069134,40.0003181 L28.9009319,40.0003181 L29.3972329,40.500159 L29.3972329,36.436452 C29.3972329,36.1603975 29.1750314,35.9366111 28.9009319,35.9366111 L25.0407031,35.9366111 C24.7666036,35.9366111 24.5444021,36.1603975 24.5444021,36.436452 L24.5444021,40.500159 L25.0407031,40.0003181 L19.1932852,40.0003181 L19.6895861,40.500159 L19.6895861,31.0771573 C19.6895861,30.8011027 19.4673846,30.5773163 19.1932852,30.5773163 L16.4972791,30.5773163 L16.8636792,31.4143071 L19.5596852,28.4424567 C19.6432504,28.3503415 19.6895861,28.23008 19.6895861,28.1053069 L19.1932852,28.0781115 Z M19.1932852,28.1053069 L18.8268851,27.7681571 L16.130879,30.7400074 C15.8398619,31.0608002 16.0658321,31.5769982 16.4972791,31.5769982 L19.1932852,31.5769982 L18.6969842,31.0771573 L18.6969842,40.500159 C18.6969842,40.7762136 18.9191857,41 19.1932852,41 L25.0407031,41 C25.3148025,41 25.537004,40.7762136 25.537004,40.500159 L25.537004,36.436452 L25.0407031,36.936293 L28.9009319,36.936293 L28.404631,36.436452 L28.404631,40.500159 C28.404631,40.7762136 28.6268325,41 28.9009319,41 L34.8069134,41 C35.0810128,41 35.3032143,40.7762136 35.3032143,40.500159 L35.3032143,31.0771573 L34.8069134,31.5769982 L37.5027209,31.5769982 C37.9341679,31.5769982 38.1601381,31.0608002 37.869121,30.7400074 L35.1733134,27.7683759 L34.8069134,28.1055257 L35.3032143,28.1055257 C35.3032143,27.802057 35.0810128,27.5782706 34.8069134,27.5782706 L34.7820437,28.0781115 L35.1484438,27.7409617 L27.3664001,19.1626911 C27.169613,18.9457696 26.830387,18.9457696 26.6335999,19.1626911 L18.8515562,27.7409617 L19.2179563,28.0781115 L19.2179563,27.5782706 C18.9191857,27.5782706 18.6969842,27.802057 18.6969842,28.0781115 L19.1932852,28.1053069 Z"
									id="svg-icon-home"></path>
							</g>
						</g>
					</g>
				</svg>
			</a>
			<?php
		} else {
			?>
			<a href="/explore" data-pjax="" class="c-active float-left navigation-logo c-ga-click" rel="nofollow"
				 data-js-ga-click="top-menu logo">
				<?php
				echo random_logo_generator();
				?>
			</a>
			<?php
		}
		?>
		<div class="nav-right-hand float-left clearfix">
			<?php if (!wp_is_mobile()) {
				?>
				<nav
					class="nav-header-menu js-header-navigation float-left js-dropdown-parent dropdown-container"
					style="position: static">
					<div class="submenu js-submenu"
							 id="submenu-about-us">
						<ul class="c-flexmenu">
							<li class="list-item c-active">
								<a href="/about-us" class="c-ga-click js-nav-link"
									 data-js-ga-click="top-menu open about-us">About-us</a>
							</li>
							<li class="list-item c-active active">
								<a href="/apps" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open products">Apps</a>
							</li>
							<li class="list-item c-active">
								<a href="/press-kit" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open presskit">Press
									Center</a>
							</li>
							<li class="list-item c-active">
								<a href="/jobs" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open jobs">Careers</a>
							</li>
							<li class="list-item c-active">
								<a href="/contact-us" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open contact-us">Contact
									Us</a>
							</li>
							<li class="list-item c-active">
								<a href="/research" class="c-ga-click js-nav-link"
									 data-js-ga-click="top-menu open research">Research</a>
							</li>
							<li class="list-item c-active">
								<a href="/faq" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open faq">FAQ</a>
							</li>
						</ul>
					</div>
					<div class="submenu js-submenu <?php if (is_post_type_archive('apps') || is_page('apps') || $class == 'apps') echo 'hidden'; ?>"
							 id="submenu-tutorials">
						<ul class="c-flexmenu">
							<li class="list-item pa-tutorials-anchor c-active">
								<a href="/tutorials" class="c-ga-click js-nav-link"
									 data-js-ga-click="top-menu open tutorials">Tutorials </a>
							</li>
							<li
								class="list-item c-active <?php if (is_page('clipart') || is_singular('clipart')) echo 'active'; ?>">
								<a href="/clipart" class="submenu-opener js-nav-link">Clipart</a>
							</li>
							<li
								class="list-item c-active <?php if (is_post_type_archive('backgrounds') || is_singular('backgrounds')) echo 'active'; ?>">
								<a href="/backgrounds" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open backgrounds">
									Backgrounds </a>
							</li>
							<li
								class="list-item c-active <?php if (is_post_type_archive('collage') || is_singular('collage')) echo 'active'; ?>">
								<a href="/collage" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open collage">
									Collage </a></li>
							<li class="list-item pa-contests-anchor c-active">
								<a href="/contests" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open challenges">
									Challenges </a>
							</li>
						</ul>
					</div>
				</nav>
				<?php
			} ?>
			<?php get_search_form(); ?>
			<?php if (wp_is_mobile()) : ?>
				<div class="pa-sidebar-drawer js-active-dropdown float-right js-dropdown-parent dropdown-container">
					<a href="#" class="dropdown-opener c-dropdown-opener" data-js-mode="click" data-js-position="below left">
						<svg width="24px" height="6px" id="drawer-btn-svg" viewBox="0 0 24 6" version="1.1"
								 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<defs>
								<path
									d="M292,24 C290.343146,24 289,22.6568542 289,21 C289,19.3431458 290.343146,18 292,18 C293.656854,18 295,19.3431458 295,21 C295,22.6568542 293.656854,24 292,24 Z M292,33 C290.343146,33 289,31.6568542 289,30 C289,28.3431458 290.343146,27 292,27 C293.656854,27 295,28.3431458 295,30 C295,31.6568542 293.656854,33 292,33 Z M292,42 C290.343146,42 289,40.6568542 289,39 C289,37.3431458 290.343146,36 292,36 C293.656854,36 295,37.3431458 295,39 C295,40.6568542 293.656854,42 292,42 Z"
									id="path-1"></path>
								<mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0"
											width="6" height="24" fill="white">
									<use xlink:href="#path-1"></use>
								</mask>
							</defs>
							<g id="page-2" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<g id="more-menu-icon-svg" transform="translate(-280.000000, -91.000000)" stroke="#888888"
									 stroke-width="2">
									<g id="top-navigation" transform="translate(0.000000, 68.000000)">
										<use id="more-phone-sidebar" mask="url(#mask-2)"
												 transform="translate(290.000000, 28.000000) rotate(-90.000000) translate(-290.000000, -28.000000) "
												 xlink:href="#path-1"></use>
									</g>
								</g>
							</g>
						</svg>
					</a>
					<div class="js-dropdown dropdown below left hidden">
						<ul class="header-menu phone-sidebar js-phone-sidebar to-top">
							<li class="list-item c-active">
								<a href="/feed" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open feed"> Feed </a>
							</li>
							<li class="list-item pa-explore-anchor c-active">
								<a href="/explore" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open explore">
									Explore </a>
							</li>
							<li class="list-item pa-stickers-anchor c-active">
								<a href="/stickers" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open stickers">
									Stickers </a>
							</li>
							<li class="list-item c-active">
								<a href="/hashtag/gifs" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open gifs">
									Gifs </a>
							</li>
							<li class="list-item pa-stickers-anchor c-active">
								<a href="/hashtag/drawings" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open drawings">
									Drawings</a>
							</li>
							<li class="list-item c-active">
								<a href="/search/artists" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open artists">
									Artists </a>
							</li>
							<li class="list-item c-active">
								<a href="/search/tags" class="c-ga-click js-nav-link" data-js-ga-click="top-menu open tags"> Tags </a>
							</li>
							<li class="list-item c-active"><a href="/blog" target="_blank" class="c-ga-click js-nav-link"
																								data-js-active="blog" data-js-ga-click="top-menu open blog"> Blog </a>
							</li>
							<li
								class="list-item c-active <?php if (is_post_type_archive('apps') || is_page('apps') || $class == 'apps') echo 'active'; ?>">
								<a href="/apps"> Apps </a>
							</li>
							<li class="list-item c-active"><a href="https://www.zazzle.com/picsart" class="c-ga-click js-nav-link"
																								target="_blank" data-js-ga-click="top-menu open store"> PicsArt
									Store </a>
							</li>
							<li class="list-item js-active-dropdown c-active js-dropdown-parent dropdown-container"><span
									class="js-nav-link dropdown-opener dropdown-submenu-opener c-dropdown-opener js-dropdown-submenu-opener"
									data-pjax="" data-js-submenu="submenu-tutorials" data-js-position="below left">            More            <svg
										width="5px" height="8px" viewBox="0 0 5 8" version="1.1" xmlns="http://www.w3.org/2000/svg"
										xmlns:xlink="http://www.w3.org/1999/xlink">                <g id="Page-6" stroke="none"
																																									stroke-width="1" fill="none"
																																									fill-rule="evenodd">                    <g
												id="more-icon-svg" transform="translate(-694.000000, -780.000000)" fill="#4D4D4F">                        <g
													id="more-submenu-opener-1" transform="translate(0.000000, 690.000000)">                            <g
														id="more-submenu-opener-2" transform="translate(555.000000, 7.000000)">                                <polygon
															id="Arrow-Next"
															points="142.571429 87 139 90.3333333 139.714286 91 144 87 139.714286 83 139 83.6666667"></polygon>                            </g>                        </g>                    </g>                </g>            </svg>        </span>
							</li>
							<li class="list-item c-active js-active-dropdown js-dropdown-parent dropdown-container"><span
									class="js-nav-link dropdown-opener c-dropdown-opener dropdown-submenu-opener js-dropdown-submenu-opener"
									data-js-submenu="submenu-about-us" data-js-ga-click="top-menu open about-us">               About Us                    <svg
										width="5px" height="8px" viewBox="0 0 5 8" version="1.1" xmlns="http://www.w3.org/2000/svg"
										xmlns:xlink="http://www.w3.org/1999/xlink">                <g id="Page-5" stroke="none"
																																									stroke-width="1" fill="none"
																																									fill-rule="evenodd">                    <g
												id="more-about-icon-svg" transform="translate(-694.000000, -780.000000)" fill="#4D4D4F">                        <g
													id="about-submenu-opener-1" transform="translate(0.000000, 690.000000)">                            <g
														id="about-submenu-opener-2" transform="translate(555.000000, 7.000000)">                                <polygon
															id="Arrow-Right"
															points="142.571429 87 139 90.3333333 139.714286 91 144 87 139.714286 83 139 83.6666667"></polygon>                            </g>                        </g>                    </g>                </g>            </svg>                    </span>
							</li>
						</ul>
						<div class="slide-submenu hidden js-submenu" id="submenu-about-us">
							<div class="submenu-back">
								<a href="#" class="back-to-main-dropdown js-back-to-main-dropdown">
									<svg width="20px" height="15px" viewBox="0 0 20 15" version="1.1" xmlns="http://www.w3.org/2000/svg"
											 xmlns:xlink="http://www.w3.org/1999/xlink">
										<g id="page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<g id="back-to-more-menu-icon-svg" transform="translate(-16.000000, -229.000000)"
												 fill="#4D4D4F">
												<path
													d="M16.146,236.854 L23.146,243.854 C23.341,244.049 23.658,244.049 23.853,243.854 C24.048,243.659 24.048,243.342 23.853,243.147 L17.707,237 L35.5,237 C35.775,237 36,236.775 36,236.5 C36,236.225 35.775,236 35.5,236 L17.707,236 L23.853,229.854 C24.048,229.659 24.048,229.342 23.853,229.147 C23.756,229.049 23.628,229 23.5,229 C23.372,229 23.244,229.049 23.146,229.146 L16.146,236.146 C15.951,236.342 15.951,236.658 16.146,236.854 L16.146,236.854 Z"
													id="back-to-more-icon"></path>
											</g>
										</g>
									</svg>
								</a>
							</div>
							<ul class="">
								<li class="list-item c-active"><a href="/about-us" class="c-ga-click js-nav-link" target="_blank"
																									data-js-active="about-us" data-js-submenu="submenu-about-us"
																									data-js-ga-click="top-menu open about-us"> About-us </a>
								</li>
								<li class="list-item c-active"><a href="/apps" class="c-ga-click js-nav-link" target="_blank"
																									data-js-active="apps" data-js-submenu="submenu-about-us"
																									data-js-ga-click="top-menu open products"> Apps </a>
								</li>
								<li class="list-item c-active"><a href="/press-kit" class="c-ga-click js-nav-link" target="_blank"
																									data-js-active="press-kit" data-js-submenu="submenu-about-us"
																									data-js-ga-click="top-menu open presskit"> Press Center </a>
								</li>
								<li class="list-item c-active"><a href="/jobs" class="c-ga-click js-nav-link" target="_blank"
																									data-js-active="jobs" data-js-submenu="submenu-about-us"
																									data-js-ga-click="top-menu open jobs"> Careers </a>
								</li>
								<li class="list-item c-active"><a href="/contact-us" class="c-ga-click js-nav-link" target="_blank"
																									data-js-active="contact-us" data-js-submenu="submenu-about-us"
																									data-js-ga-click="top-menu open contact-us"> Contact Us </a>
								</li>
								<li class="list-item c-active"><a href="/research" class="c-ga-click js-nav-link" target="_blank"
																									data-js-active="research" data-js-submenu="submenu-about-us"
																									data-js-ga-click="top-menu open research"> Research </a>
								</li>
								<li class="list-item c-active"><a href="/faq" class="c-ga-click js-nav-link" target="_blank"
																									data-js-active="faq" data-js-submenu="submenu-about-us"
																									data-js-ga-click="top-menu open faq"> FAQ </a>
								</li>
							</ul>
						</div>
						<div class="slide-submenu hidden js-submenu" id="submenu-tutorials">
							<div class="submenu-back">
								<a href="#" class="back-to-main-dropdown js-back-to-main-dropdown">
									<svg width="20px" height="15px" viewBox="0 0 20 15" version="1.1" xmlns="http://www.w3.org/2000/svg"
											 xmlns:xlink="http://www.w3.org/1999/xlink">
										<g id="page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<g id="back-to-more-menu-icon-svg" transform="translate(-16.000000, -229.000000)"
												 fill="#4D4D4F">
												<path
													d="M16.146,236.854 L23.146,243.854 C23.341,244.049 23.658,244.049 23.853,243.854 C24.048,243.659 24.048,243.342 23.853,243.147 L17.707,237 L35.5,237 C35.775,237 36,236.775 36,236.5 C36,236.225 35.775,236 35.5,236 L17.707,236 L23.853,229.854 C24.048,229.659 24.048,229.342 23.853,229.147 C23.756,229.049 23.628,229 23.5,229 C23.372,229 23.244,229.049 23.146,229.146 L16.146,236.146 C15.951,236.342 15.951,236.658 16.146,236.854 L16.146,236.854 Z"
													id="back-to-more-icon"></path>
											</g>
										</g>
									</svg>
								</a>
							</div>
							<ul class="">
								<li class="list-item pa-tutorials-anchor c-active"><a href="/tutorials" class="c-ga-click js-nav-link"
																																			target="_blank" data-js-active="^/tutorials"
																																			data-js-submenu="submenu-tutorials"
																																			data-js-ga-click="top-menu open tutorials">
										Tutorials </a>
								</li>
								<li
									class="list-item c-active <?php if (is_page('clipart') || is_singular('clipart')) echo 'active'; ?>">
									<a href="/clipart" class="submenu-opener js-nav-link" target="_blank"
										 data-js-submenu="submenu-clipart" data-js-active="clipart">
										Clipart </a>
								</li>

								<li
									class="list-item c-active <?php if (is_post_type_archive('backgrounds') || is_singular('backgrounds')) echo 'active'; ?>">
									<a href="/backgrounds" class="c-ga-click js-nav-link" target="_blank" data-js-active="^/backgrounds"
										 data-js-submenu="submenu-tutorials" data-js-ga-click="top-menu open backgrounds">
										Backgrounds </a>
								</li>
								<li
									class="list-item c-active <?php if (is_post_type_archive('collage') || is_singular('collage')) echo 'active'; ?>">
									<a href="/collage" class="c-ga-click js-nav-link" target="_blank" data-js-active="hashtag/collage"
										 data-js-submenu="submenu-tutorials" data-js-ga-click="top-menu open collage"> Collage </a>
								</li>
								<li class="list-item pa-contests-anchor c-active">
									<a href="/contests" class="c-ga-click js-nav-link" target="_blank"
										 data-js-ga-click="top-menu open challenges">
										Challenges </a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</nav>
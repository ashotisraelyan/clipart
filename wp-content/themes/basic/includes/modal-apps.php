<?php
if ($detect->isMobile() && !$detect->isTablet()) {
	?>
	<div id="apps_modal" class="mobile-modal get-the-app-popup">
		<div class="modal-header">
			<button class="close-mobile-modal">
				<img src="<?php echo get_template_directory_uri(); ?>/img/close.svg">
			</button>
		</div>
		<div class="content">
			<div class="download-text">
				<p class="get-the-the-app-label">Download <br> <strong>PicsArt</strong> app for free</p>
			</div>
			<div class="uk-clearfix pa-store-buttons"></div>
		</div>
	</div>
	<?php
} else {
	?>
	<div id="apps_modal" class="modal fade get-the-app-popup" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-header-container clearfix">
						<h3 class="get-the-app-for-free">Get the App for free</h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<img src="<?php echo get_template_directory_uri(); ?>/img/close.svg">
						</button>
					</div>
				</div>
				<div class="modal-body">
					<div class="content">
						<p class="get-the-the-app-label">Download <br> <strong>PicsArt</strong> app for free</p>
						<div class="uk-clearfix pa-store-buttons"></div>
					</div>
					<form id="get_app_form" type="post" class="uk-form c-submit-phone-number" autocomplete="off">
						<label for="phone-number">Send a download link to my phone:</label>

						<input type="tel" id="phone-number" name="phone_number">
						<button data-js-ga-click="get-app get-link source-bottom-section" type="submit"
										class="uk-button c-ga-click uk-float-right"><span>Send link</span></button>
						<span class="js-validation-message validation-message"></span>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php
}

?>

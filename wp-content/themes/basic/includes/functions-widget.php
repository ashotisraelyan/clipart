<?php

//Front page clipart category widget

function register_clipart_front_widget() {
    register_widget('clipart_category_front_widget');
}

add_action('widgets_init', 'register_clipart_front_widget');

class clipart_category_front_widget extends WP_Widget {

  /**
   * Holds widget settings defaults, populated in constructor.
   *
   * @var array
   */
  protected $defaults;

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    $this->defaults = array();
    $widget_ops = array();
    $control_ops = array();

    parent::__construct('user-profile', __('Clipart Frontpage Widget', 'basic'), $widget_ops, $control_ops);
  }

  function widget($args, $instance) {
    global $wpdb;
    extract($args);
    /** Merge with defaults */
    $instance = wp_parse_args((array) $instance, $this->defaults);
    if (!isset($instance['select_category'])) {
      echo __('Posts not found', 'bootstrap-basic');
      return;
    }

    $cat_id = (int) $instance['select_category'];
		echo get_front_posts($cat_id);

    ?>
    <?php
  }

  function form($instance) {
    $instance = wp_parse_args((array) $instance, $this->defaults);
    $categories = get_categories('exclude=1,2,3');
    ?>
    <p>
      <label for="select_category">Select category</label>
      <select id="<?php echo $this->get_field_id('select_category'); ?>" name="<?php echo $this->get_field_name('select_category'); ?>" class="select_category" style="width:100%;">
        <?php
        foreach ($categories as $category) {
          if($category->term_id == $instance['select_category']) { ?>
            <option class="category_options" selected value="<?php echo $category->term_id; ?>"><?php echo $category->name; ?></option>
            <?php
          } else { ?>
            <option class="category_options" value="<?php echo $category->term_id; ?>"><?php echo $category->name; ?></option>
            <?php
          }
        }
        ?>
      </select>
    </p>
    <?php
  }

  function update($new_instance, $old_instance) {
    $instance = array();
    $instance['select_category'] = ( ! empty( $new_instance['select_category'] ) ) ? strip_tags( $new_instance['select_category'] ) : '';
    $instance['select_count'] = ( ! empty( $new_instance['select_count'] ) ) ? strip_tags( $new_instance['select_count'] ) : '';
    return $instance;
  }
}

function admin_clipart_media_enqueue(){
  wp_enqueue_media();
  wp_enqueue_script( 'front-get-widget-script', get_template_directory_uri() . '/js/script.js', array(), '3.7.4', true );
}

add_action('admin_enqueue_scripts', 'admin_clipart_media_enqueue');


//Front page get app widget

add_action('widgets_init', 'front_get_app_widget');

function front_get_app_widget() {
	register_widget( 'front_get_app_widget' );
}

class front_get_app_widget extends WP_Widget{

	/**
   * Holds widget settings defaults, populated in constructor.
   *
   * @var array
   */
  protected $defaults;

  /**
   * Register widget with WordPress.
   */

function __construct() {
    $widget_ops = array( 'classname' => 'front-get-app' );
    $control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'front-get-app-widget' );
		parent::__construct('front-get-app-widget', __('Frontpage Get App', 'basic'), $widget_ops, $control_ops);
}

public function widget($args, $instance){
  extract( $args );
  ?>

  <div class="get_app" style="background: url(<?php echo $instance['image_uri']; ?>);">
    <div class="app_container main-container">
      <h3><?php echo $instance['title']; ?></h3>
      <form class="uk-form c-submit-phone-number" autocomplete="off">
        <label for="phone-number"><?php echo $instance['description']; ?></label>
        <span class="js-validation-message validation-message"></span>
        <input type="text" id="phone-number" class="pa-input" placeholder="Type your phone number">
        <button data-js-ga-click="get-app get-link source-bottom-section" class="c-ga-click uk-button pa-btn">Get link</button>
      </form>
      <div class="uk-clearfix pa-store-buttons">
        <a href="//itunes.apple.com/us/app/picsart-photo-studio/id587366035" class="apple-store c-ga-click" data-js-ga-click="get-app appstore source-bottom-section"></a>
    		<a href="//play.google.com/store/apps/details?id=com.picsart.studio" class="google-store c-ga-click" data-js-ga-click="clipart playstore source-bottom-section"></a>
    		<a href="//www.microsoft.com/en-us/store/p/picsart-photo-studio/9wzdncrfj10m" class="microsoft-store c-ga-click" data-js-ga-click="clipart microsoft source-bottom-section"></a>
      </div>
    </div>
  </div>
<?php
}

function update($new_instance, $old_instance) {
  $instance = $old_instance;
  $instance['title'] = strip_tags( $new_instance['title'] );
  $instance['description'] = strip_tags ( $new_instance['description']);
  $instance['image_uri'] = strip_tags( $new_instance['image_uri'] );

  return $instance;
}

public function form($instance){ ?>

  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'basic'); ?></label><br />
    <input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description', 'basic');?></label><br />
    <input type="text" name="<?php echo $this->get_field_name('description'); ?>" id="<?php echo $this->get_field_id('title'); ?>" value="<?php echo $instance['description']; ?>" class="widefat" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('image_uri'); ?>">Image</label><br />
    <img class="custom_media_image" src="<?php if(!empty($instance['image_uri'])){echo $instance['image_uri'];} ?>" style="margin:0;padding:0;max-width:100px;float:left;display:inline-block" />
      <input type="text" class="widefat custom_media_url" name="<?php echo $this->get_field_name('image_uri'); ?>" id="<?php echo $this->get_field_id('image_uri'); ?>" value="<?php echo $instance['image_uri']; ?>">
      <input type="button" value="<?php _e( 'Upload Image', 'themename' ); ?>" class="button custom_media_upload" id="custom_image_uploader"/>
    </p>
    <?php
  }
}
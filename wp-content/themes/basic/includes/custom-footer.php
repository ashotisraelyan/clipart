<footer class="footer" <?php if (is_404()) {
	echo 'style="display:none;"';
} ?>>
	<div class=" main-container uk-clearfix">
		<div class="pa-footer-block uk-list">
			<?php
			wp_nav_menu(array(
				'theme_location' => 'bottom_left',
				'menu_id' => 'footer-left-menu',
				'container' => false,
			));
			?>
		</div>
		<div class="pa-footer-block uk-list">
			<?php
			wp_nav_menu(array(
				'theme_location' => 'bottom_center',
				'menu_id' => 'footer-center-menu',
				'container' => false,
			));
			?>
		</div>
		<div class="pa-footer-block uk-list">
			<?php
			wp_nav_menu(array(
				'theme_location' => 'bottom_right',
				'menu_id' => 'footer-right-menu',
				'container' => false,
			));
			?>
		</div>

		<div class="pa-footer-block uk-list">
			<ul class="social-menu">
				<li class="list-item">
					<div class="follow-us">Follow Us:</div>
					<div class="pa-nav-follow-container uk-clearfix">
						<a target="_blank" href="https://www.facebook.com/picsart"
							 class="uk-icon-small uk-icon-hover uk-icon-facebook"></a>
						<a target="_blank" href="https://twitter.com/PicsArtStudio"
							 class="uk-icon-small uk-icon-hover uk-icon-twitter"></a>
						<a target="_blank" href="https://plus.google.com/+PicsArt/posts"
							 class="uk-icon-small uk-icon-hover uk-icon-google-plus"></a>
						<a target="_blank" href="https://youtube.com/picsart"
							 class="uk-icon-small uk-icon-hover uk-icon-youtube"></a>
						<a target="_blank" href="https://instagram.com/picsart"
							 class="uk-icon-small uk-icon-hover uk-icon-instagram"></a>
						<a target="_blank" href="//picsart.com/blog/feed" class="uk-icon-small uk-icon-hover uk-icon-rss"></a>
					</div>
				</li>
				<li class="list-item">
					<div class="download-picsart">Download Picsart</div>
					<div class="uk-clearfix pa-store-buttons">
						<a href="//itunes.apple.com/us/app/picsart-photo-studio/id587366035" class="apple-store c-ga-click"
							 data-js-ga-click="get-app appstore source-bottom-section"></a>
						<a href="//play.google.com/store/apps/details?id=com.picsart.studio" class="google-store c-ga-click"
							 data-js-ga-click="get-app playstore source-bottom-section"></a>
					</div>
				</li>
			</ul>
		</div>

	</div>
	<span class="footer-copyright">© Copyright <?php echo date('Y'); ?> PicsArt</span>

</footer>
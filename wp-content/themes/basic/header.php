<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<title> <?php echo pa_render_meta_title(); ?> </title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="mask-icon" href="/picsart-mask-icon.svg" color="#262626 ">
	<link rel="apple-touch-icon" href="/fav-icon196.png">
	<link rel="apple-touch-icon" sizes="16x16" href="/fav-icon16.png">
	<link rel="apple-touch-icon" sizes="32x32" href="/fav-icon32.png">
	<link rel="apple-touch-icon" sizes="48x48" href="/fav-icon48.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/fav-icon96.png">
	<link rel="icon" type="image/png" sizes="192x192" href="/fav-icon192.png">
	<link rel="icon" type="image/png" sizes="196x196" href="/fav-icon196.png">
	<?php if (!is_post_type_archive('apps')): ?>
		<meta property="al:ios:url" content="<?php echo pa_render_meta_deeplink(); ?>">
		<meta property="al:ios:app_store_id" content="587366035">
		<meta property="al:ios:app_name" content="PicsArt">
		<meta property="al:android:url" content="<?php echo pa_render_meta_deeplink(); ?>">
		<meta property="al:android:app_name" content="PicsArt">
		<meta property="al:android:package" content="com.picsart.studio">
	<?php endif; ?>

	<meta property="og:site_name" content="PicsArt">
	<meta property="og:title" content="<?php echo pa_render_og_title(); ?>">
	<meta property="og:image" content="<?php echo pa_render_share_image_url(); ?>">
	<meta property="og:type" content="picsartphotostudio:photo">
	<?php
	if (is_post_type_archive('apps')) {
		$og_description = 'Download More Cool Apps By PicsArt';
	} elseif (is_tax('picsart_apps_subtype', 'picsart-photo-studio')) {
		$og_description = 'Get The Best Photo Studio App By PicsArt';
	} else {
		$og_description = 'Make Awesome Pictures';
	}
	?>
	<meta property="og:description" content="<?php echo $og_description; ?>">
	<meta property="fb:app_id" content="108569252539536">
	<meta name="twitter:card" content="photo">
	<meta name="twitter:site" content="@PicsArtStudio">
	<meta name="twitter:url" content="https://picsart.com">
	<meta name="twitter:title" content="PicsArt Photo Studio">
	<meta name="twitter:description" content="<?php echo $og_description; ?>">
	<meta name="description" content="<?php echo pa_render_meta_description(); ?>">
	<meta name="google-site-verification" content="yFLvQHMKUSbDw5rOjIXaF4u_sNVNykinXHJFsOMDoNs">
	<?php
	global $wp;
	$current_url = home_url(add_query_arg(array(), $wp->request));
	?>
	<link rel="canonical" href="<?php echo $current_url; ?>/">

	<?php wp_head(); ?>

</head>
<?php
$body_class = '';
if (wp_is_mobile()) {
	$body_class = "phone";
} else {
	$body_class = 'web';
}
?>
<body <?php body_class($body_class); ?>>
<div class="wrapper clearfix">
	<?php include 'includes/custom-navigation.php'; ?>
	<?php
	if (is_404()) {
		$main_class = '';
	} else {
		if (wp_is_mobile()) {
			$main_class = 'mobile_main';
		} else {
			$main_class = 'main';
			if (is_tax('picsart_apps_subtype')) {
				$main_class = 'apps-term';
			}
		}
	}

	?>
	<div class="<?php echo $main_class; ?> maxwidth clearfix">

		<!-- BEGIN content -->

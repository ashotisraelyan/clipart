<?php get_header(); ?>
	<div class="main-container">
		<div class="search-results-page">
			<?php

			$query_clipart = new WP_Query(array('s' => get_query_var('q'), 'post_type' => 'post', 'post_status' => 'publish'));
			$query_backgrounds = new WP_Query(array('s' => get_query_var('q'), 'post_type' => 'backgrounds', 'post_status' => 'publish'));
			$query_collage = new WP_Query(array('s' => get_query_var('q'), 'post_type' => 'collage', 'post_status' => 'publish'));

			if ($query_clipart->post_count == 0 && $query_backgrounds->post_count == 0 && $query_collage->post_count == 0) {
				 ?>
				<div class="pa-standard-container main-container not-found-section not-found-404">
					<input type="hidden" class="c-not-found" data-js-not-found="*">
					<a href="/" class="logo"></a>
					<img class="illustration" src="<?php echo get_template_directory_uri(); ?>/img/404.png"
							 srcset="<?php echo get_template_directory_uri(); ?>/img/404@2x.png 2x"
							 alt="404">
					<p class="message"> <?php echo __('Whoopsie - the '.get_query_var('q').' you are looking for magically disappeared. Here are the most trending categories... Or you can search for Donald Trump Clipart or something...', 'basic'); ?> </p>
					<a href="/clipart" class="btn-rounded-big">Explore clipart</a>
					<form class="c-large-search large-search" action="<?php echo esc_url(home_url('/')); ?>shop-search/">
						<input class="" type="search" name="q" value="" placeholder="Search cliparts, collages, backgrounds…"
									 autocomplete="off"></form>

					<div class=" ">
						<div>
							<ul class="tag-list c-gradient">
								<?php
								$categories = get_categories();
								$rand_keys = array_rand($categories, 8);

								foreach ($rand_keys as $key) {
									?>
									<li class="item">
										<h3>
											<a href="/clipart/#<?php echo $categories[$key]->slug; ?>">
												<div class="tag"
														 style="background-position: 0px 0px;"><?php echo $categories[$key]->name; ?></div>
											</a>
										</h3>
									</li>
									<?php
								}
								?>
							</ul>
						</div>
					</div>
				</div>
				<?php
			} else {
				pa_get_search_results($query_clipart, 'clipart');
				pa_get_search_results($query_backgrounds, 'background');
				pa_get_search_results($query_collage, 'collage');
			}
			?>

		</div> <!-- .search-results -->
	</div> <!-- .container -->

<?php get_footer();



<?php
header('Content-Type: application/json');

$args = array(
	'offset' => 0,
	'posts_per_page' => -1,
	'orderby' => 'date',
	'order' => 'DESC',
	'post_type' => 'backgrounds',
	'post_status' => 'publish',
	'suppress_filters' => true,
	'tax_query' => array(
		'taxonomy' => 'picsart_backgrounds_subtype',
		'field' => 'slug',
		'terms' => 'backgrounds-front'
	)
);

$posts = get_posts($args);

$response = array(
	'backgrounds' => array()
);

foreach ($posts as $post) {
	$deeplink_url = get_post_meta($post->ID, 'Deeplink Url', true);

	$package_uid = substr($deeplink_url, strpos($deeplink_url, 'id=') + 3);

	if ($deeplink_url && $deeplink_url != '') {
		$output = array(
			'title' => $post->post_title,
			'link' => get_the_permalink($post->ID),
			'deeplink' => $deeplink_url,
			'package_uid' => $package_uid
		);
		array_push($response['backgrounds'], $output);
	}
}
echo json_encode($response);
<?php get_header(); ?>

	<div id="pjax-container" data-uk-observe="">
		<?php

		$query = get_query_var('q');

		if (strpos($query, 'clipart') != false) {
			$not_found = 'clipart';
			$url = '/clipart';
		} elseif (strpos($query, 'collage') != false) {
			$not_found = 'collage';
			$url = '/collage';
		}	elseif (strpos($query, 'backgrounds') != false) {
			$not_found = 'background';
			$url = '/backgrounds';
		} else {
			$url = '/explore';
			$not_found = 'thing';
		}

		?>
		<!--page types ['404','404_photo','404_feed']-->
		<div class="pa-standard-container main-container not-found-section not-found-404">
			<input type="hidden" class="c-not-found" data-js-not-found="*">
			<a href="/" class="logo"></a>
			<img class="illustration" src="<?php echo get_template_directory_uri(); ?>/img/404.png"
					 srcset="<?php echo get_template_directory_uri(); ?>/img/404@2x.png 2x"
					 alt="404">
			<p class="message"> <?php echo __('Whoopsie - the '.$not_found.' you are looking for magically disappeared. Here are the most trending categories... Or you can search for Donald Trump Clipart or something...', 'basic'); ?> </p>
			<a href="<?php echo $url; ?>" class="btn-rounded-big">Explore <?php echo $not_found; ?></a>
			<form class="c-large-search large-search" action="<?php echo esc_url(home_url('/')); ?>shop-search/">
				<input class="" type="search" name="q" value="" placeholder="Search cliparts, collages, backgrounds…"
							 autocomplete="off"></form>

			<div class=" ">
				<div>
					<ul class="tag-list c-gradient">
						<?php
						$categories = get_categories();
						$rand_keys = array_rand($categories, 8);

						foreach ($rand_keys as $key) {
							?>
							<li class="item">
								<h3>
									<a href="/clipart/#<?php echo $categories[$key]->slug; ?>">
										<div class="tag"
												 style="background-position: 0px 0px;"><?php echo $categories[$key]->name; ?></div>
									</a>
								</h3>
							</li>
							<?php
						}
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php get_footer();
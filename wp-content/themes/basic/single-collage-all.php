<?php
header('Content-Type: application/json');

$args = array(
	'offset' => 0,
	'posts_per_page' => -1,
	'orderby' => 'date',
	'order' => 'DESC',
	'post_type' => 'collage',
	'post_status' => 'publish',
	'suppress_filters' => true,
	'tax_query' => array(
		'taxonomy' => 'picsart_collage_subtype',
		'field' => 'slug',
		'terms' => 'frames'
	)
);

$posts = get_posts($args);

$response = array(
	'collage' => array()
);

foreach($posts as $post) {
	$deeplink_url = get_post_meta($post->ID, 'Deeplink Url', true);

	$package_uid = substr($deeplink_url, strpos($deeplink_url, 'id=') + 3);


	if($deeplink_url && $deeplink_url != '') {
		$output = array(
			'title' => $post->post_title,
			'link' => get_the_permalink($post->ID),
			'deeplink' => $deeplink_url,
			'package_uid' => $package_uid
		);
		array_push($response['collage'], $output);
	}
}
echo json_encode($response);
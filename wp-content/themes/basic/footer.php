</div>

<?php if (!is_tax('picsart_apps_subtype', 'picsart-photo-studio')) {
	include 'includes/custom-footer.php';
} ?>

</div> <!-- .wrapper -->
<div class="pa-overlay-mask"></div>
<!--
<?php
//if(!wp_is_mobile()){
?>
		<a href="https://picsart.com/sign-in" class=" pa-photo-upload-button">
			<div class="icon-upload-photo"></div>
		</a>
		<?php
//}
?> -->

<div class="scroll-top-wrapper">
	<span class="icon-to-top-page">
	</span>
</div>

<?php
if (wp_is_mobile()) {

	if (get_post_meta(get_the_ID(), 'Deeplink Url', true)) {
		$deeplink_url = get_post_meta(get_the_ID(), 'Deeplink Url', true);
		if (is_post_type_archive('collage')) {
			$deeplink_url = 'picsart://shop?category=collage_frames';
		} elseif (is_post_type_archive('backgrounds')) {
			$deeplink_url = 'picsar://backgrounds';
		}
	} elseif (is_post_type_archive('collage') || is_singular('collage')) {
		$deeplink_url = 'picsart://shop?category=collage_frames';
	} elseif (is_post_type_archive('backgrounds') || is_singular('backgrounds')) {
		$deeplink_url = 'picsart://shop?category=backgrounds';
	} else {
		$deeplink_url = 'picsart://explore';
	}
	?>
	<input type="hidden" name="deeplink-url" id="deeplink-url" value="<?php echo $deeplink_url; ?>"/>
	<?php
}
wp_footer(); ?>

<?php if (!is_preview()) : ?>
	<script type="text/javascript" async="async">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-28323291-1']);
    _gaq.push(['_setDomainName', 'picsart.com']);
    _gaq.push(['_trackPageview']);

    (function () {
      var ga = document.createElement('script');
      ga.type = 'text/javascript';
      ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(ga, s);
    })();
	</script>

<?php endif;

$micro_data = get_post_meta(get_the_ID(), 'Micro Data', true);

if ($micro_data) {
	echo '<script type="application/ld+json">' . $micro_data . '</script>';
}
?>

</body>
</html>

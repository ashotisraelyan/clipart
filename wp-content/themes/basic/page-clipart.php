<?php get_header(); ?>
<main class="content main-page">
	<?php echo get_featured_block('post'); ?>
	<div class="main-container">
		<div class="main-page-content">
			<div class="about">
				<?php
				if (is_active_sidebar('front_middle')) {
					dynamic_sidebar('front_middle');
				}
				?>
			</div>
			<div class="clipart-container">
				<div class="cliparts">
					<ul>
						<?php
						if (is_active_sidebar('front_bottom')) {
							dynamic_sidebar('front_bottom');
						}
						?>
					</ul>
				</div>

			</div>
		</div>
		<div class="frontpage-sidebar">
			<?php echo get_frontpage_sidebar(); ?>
		</div>
	</div>


	<div class="clearfix"></div>
	<?php
	if (is_active_sidebar('get_app_area')) {
		dynamic_sidebar('get_app_area');
	}
	?>
</main>
<?php include 'includes/modal-top.php' ?>
<!-- END #content -->
<?php get_footer(); ?>

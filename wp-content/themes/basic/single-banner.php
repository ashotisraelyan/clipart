<html>
<head>
	<title>
		<?php
		if (is_404()) {
			echo 'Error';
		} else {
			wp_title('- PicsArt', true, 'right');
		}
		?>
	</title>
	<meta property="title" content="<?php echo wp_title('- PicsArt', true, 'right'); ?>">
	<?php
	$meta_description = get_post_meta(get_the_ID(), 'Banner Meta Description', true);

	if (!$meta_description) {
		$meta_description = custom_get_excerpt(get_the_ID(), 21);
	}
	?>
	<meta name="description" content="<?php echo $meta_description; ?>">
</head>

<div id="content">
	<?php
	while (have_posts()) {
		the_post();
		the_content();
	}
	?>
</div>
</html>
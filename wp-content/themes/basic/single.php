<?php
get_header();
?>
<div class="col-md-12 post">
	<?php
	while (have_posts()) {
		the_post();
		if (get_post_meta(get_the_ID(), 'Page Layout', true) == 1) {
			?>
			<div class="basic-layout">
				<?php
				if (get_the_post_thumbnail(get_the_ID())) :
					?>
					<div
						class="post_header main-container uk-cover-background uk-position-relative uk-container-center uk-padding-remove about-cover"
						style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>)">
						<div class="uk-position-cover uk-padding-remove uk-flex uk-flex-center uk-flex-middle">
							<div class="header-title main-container">
								<?php
								$color = '#000';
								if (get_post_meta(get_the_ID(), 'Video Text Color', true)) {
									$color = get_post_meta(get_the_ID(), 'Video Text Color', true);
								}
								?>
								<h1 style="color: <?php echo $color; ?>;!important;" class="title"><?php the_title(); ?></h1>
								<?php
								if (wp_is_mobile()) {
									$get_app_class = '';
									if (get_post_meta(get_the_ID(), 'Deeplink Url', true)) {
										$deeplink_url = get_post_meta(get_the_ID(), 'Deeplink Url', true);
									} else {
										$deeplink_url = 'picsart://explore';
									}
								} else {
									$get_app_class = 'js-get-app-modal-top';
									$deeplink_url = '#';
								}
								?>
								<a href="<?php echo $deeplink_url; ?>" data-js-ga-click="clipart get-clipart source-top-menu"
									 class="get-package c-ga-click <?php echo $get_app_class; ?>">get clipart</a>
							</div>
						</div>
					</div>

				<?php endif; ?>
				<div class="text-container main-container">
					<div class="text">
						<?php
						if (get_post_meta(get_the_ID(), 'Post Top Share Box', true) == 1) :
							include 'includes/share-box.php';
						endif;
						?>
						<?php the_content(); ?>
						<?php
						if (get_post_meta(get_the_ID(), 'Post Bottom Share Box', true) == 1) :
							echo '<div class="clearfix" style="margin-bottom:25px;"></div>';
							include 'includes/share-box.php';
						endif;
						?>
						<div class="clearfix"></div>
					</div>
					<div class="sidebar">
						<?php echo get_clipart_sidebar(get_the_ID()); ?>
					</div>
				</div>
				<div class="clearfix"></div>
				<?php echo get_basic_get_app_block(); ?>
			</div>

			<?php
		} else {
			if (get_the_post_thumbnail(get_the_id()) || custom_get_post_attachment(get_the_ID())) {
				?>
				<div class="post_header <?php if (!wp_is_mobile()) {
					echo 'js-post-header';
				} ?>">
					<?php
					if (wp_is_mobile()) {
						?>
						<div class="mobile-header-img">
							<img alt="<?php echo the_title(); ?>" src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>"/>
						</div>
						<?php
					} else {
						?>
						<video poster="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" autoplay muted loop>
							<source src="<?php echo custom_get_post_attachment(get_the_ID()); ?>" type="video/mp4">
						</video>
						<?php
					}
					?>
					<div class="title-holder main-container">
						<?php
						$color = '#000';
						if (get_post_meta(get_the_ID(), 'Video Text Color', true)) {
							$color = get_post_meta(get_the_ID(), 'Video Text Color', true);
						}
						?>
						<h1 style="color: <?php echo $color; ?>;" class="title"><?php the_title(); ?></h1>
						<?php
						if (wp_is_mobile()) {
							$get_app_class = '';
							if (get_post_meta(get_the_ID(), 'Deeplink Url', true)) {
								$deeplink_url = get_post_meta(get_the_ID(), 'Deeplink Url', true);
							} else {
								$deeplink_url = 'picsart://explore';
							}
						} else {
							$get_app_class = 'js-get-app-modal-top';
							$deeplink_url = '#';
						}
						?>
						<a href="<?php echo $deeplink_url; ?>" data-js-ga-click="clipart get-clipart source-top-menu"
							 class="get-package c-ga-click <?php echo $get_app_class; ?>">get clipart</a>
					</div>
				</div>
				<?php
			}
			?>

			<div class="text">
				<div class="text-container">
					<?php the_content(); ?>
				</div>

			</div>
			<?php
		}
	}
	?>
</div>
<div class="clearfix"></div>
<?php include 'includes/modal-top.php' ?>
<?php include 'includes/modal-bottom.php' ?>

<?php get_footer(); ?>

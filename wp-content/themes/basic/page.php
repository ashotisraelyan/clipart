<?php
require_once 'includes/mobile_detect.php';
$detect = new Mobile_Detect();

get_header(); ?>

<?php
if (have_posts()) :
	while (have_posts()) :
		the_post();
		if (is_page('apps')) {
			$class = 'products-top-container';
		} else {
			$class = 'picsart-photo-studio';
		}
		?>
		<div class="main-page" id="<?php echo $class; ?>">
			<?php the_content(); ?>
			<?php
			if (!is_page('apps') && is_page('picsart-photo-studio')) {
				echo pa_apps_get_features(get_the_ID());
			}
			if (!is_page('apps')) {
				$app_name = 'picsart';
				echo pa_apps_get_now_section($app_name);
				if (is_page('picsart-photo-studio')) {
					echo pa_get_suggested_apps(get_the_ID());
				}

				if (!is_page('picsart-photo-studio')) {
					echo pa_get_apps_pagination(get_the_ID());
				}
			}
			?>
		</div>
	<?php endwhile;
endif;
?>

<?php include 'includes/modal-apps.php'; ?>

<?php get_footer(); ?>

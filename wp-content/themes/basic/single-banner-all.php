<?php
header('Content-type: application/json');

$args = array(
	'offset' => 0,
	'posts_per_page' => -1,
	'orderby' => 'date',
	'order' => 'DESC',
	'post_type' => 'banner',
	'post_status' => 'publish',
	'suppress_filters' => true,
	array(
		'taxonomy' => 'picsart_banner_subtype',
		'field' => 'slug',
		'terms' => 'hashtag'
	)
);

$posts = get_posts($args);
$response = array(
	'hashtags' => array()
);

foreach ($posts as $post) {
	$meta_description = get_post_meta($post->ID, 'Banner Meta Description', true);

	if (!$meta_description) {
		$meta_description = custom_get_excerpt($post->ID, 21);
	}

	$output = array(
		'slug' => $post->post_name,
		'content' => $post->post_content,
		'title' => $post->post_title . ' - PicsArt',
		'description' => $meta_description

	);
	array_push($response['hashtags'], $output);
}
echo json_encode($response);
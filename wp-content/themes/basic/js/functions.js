(function (exports) {

  var Analytic = {
    trigger: function (eventDataList) {
      if (typeof _gaq != 'undefined') {
        _gaq.push(['_trackEvent'].concat(eventDataList));
        console.log('Analytics:', eventDataList.join(','));
      } else {
        console.log('Analytics:', eventDataList.join(','));
      }
    }
  };
  exports.Analytic = Analytic;
})(this);

(function ($) {
  $(document).ready(function ($) {

      var $body = $('body');
      var $web = $('.web');

      $(document).on("scroll", function () {
        $(".scroll-top-wrapper").toggleClass("show", $(window).scrollTop() > 100);
        $("#top-nav").toggleClass("floating", $(window).scrollTop() > 50);
      });

      $('.scroll-top-wrapper').click(function () {
        $('body,html').animate({scrollTop: 0}, 200);
      });

      $('.js-get-app-modal-top').on('click', function (e) {
        e.preventDefault();
        $("#top_modal").modal({
          "keyboard": true,
          "show": true
        });
      });

      $('.js-get-app-modal-bottom').on('click', function (e) {
        e.preventDefault();
        $("#bottom_modal").modal({
          "keyboard": true,
          "show": true
        });
      });

      $('.js-get-app-modal-apps').on('click', function (e) {
        e.preventDefault();
        var $urls = $(this).data('app-urls');
        var $appLinks = getAppSuggestions($urls.split(' '));
        $('#apps_modal').find('.pa-store-buttons').html($appLinks);
        $('#apps_modal').modal({
          "keyboard": true,
          "show": true
        });
      });

      $("#phone-number").intlTelInput({
        geoIpLookup: function (callback) {
          $.get("https://ipinfo.io", function () {
          }, "jsonp").always(function (resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            callback(countryCode);
          });
        },
        initialCountry: 'auto',
        utilsScript: '/wp-content/themes/basic/js/telUtils.js'
      });

      $('.js-get-app-modal-apps-mobile').on('click', function () {
        var $urls = $(this).data('app-urls');
        var $appLinks = getAppSuggestions($urls.split(' '));
        $('#apps_modal').find('.pa-store-buttons').html($appLinks);
        $('body').toggleClass('modal-open');
        $('html').toggleClass('overlay-active');
      });

      $('.close-mobile-modal').on('click', function () {
        $('body').removeClass('modal-open');
        $('html').removeClass('overlay-active');
      });

      $('.c-body-no-scroll').click(function () {
        $(this).toggleClass('active');
        $("body").toggleClass('mobile-menu-opened');
        $('.uk-offcanvas').toggleClass('uk-hidden', 500);
      });

      function getAppSuggestions(apps) {
        var $output = '';
        var $android = new RegExp('google');
        var $apple = new RegExp('apple');
        $.each(apps, function (index, app) {
          if ($android.test(app)) {
            $output += '<a href="' + app + '" class="android-store"></a>';
          } else if ($apple.test(app)) {
            $output += '<a href="' + app + '" class="apple-store"></a>';
          } else {
            $output += '<a href="' + app + '" class="windows-store"></a>';
          }
        });
        return $output;
      }

      $(document).on('click', '.c-ga-click', function () {
        Analytic.trigger($(this).attr('data-js-ga-click').split(' '));
      });

      $('.c-submit-phone-number').submit(function (e) {
        e.preventDefault();
        e.stopPropagation();

        $('.js-validation-message').removeClass('error success').text('');
        var $phone_number = $(this).find('#phone-number').val();
        $.post('/get-app-sms', {
          phone_number: $phone_number
        }, function (data) {
          if (data.status === 'success') {
            $(".pa-input").val();
          }
          $('.js-validation-message').addClass(data.status).text(data.message);
        });
      });

    var widgetTitle = $('.js-about').find('.widgettitle');

    widgetTitle.replaceWith("<h1 class='widgettitle'>" + widgetTitle.text() + "</h1>");

      $('.js-about').find('.widgettitle').replaceWith("<h1 class='widgettitle'>" + widgetTitle.text() + "</h1>");

      $('.c-flexmenu').flexMenu({
        threshold: 0,
        cutoff: 0,
        showOnHover: !0,
        linkText: "More"
      });

      $('.c-search-input').on('blur', function () {
        $('html').removeClass('overlay-active-scrollable');
        $('#nav-search-field').removeClass('over-all active');
        $('.modal-closer').addClass('hidden');
        $('.c-nav-search-container').find('label').addClass('hidden');
      }).on('focus', function () {
        $('html').addClass('overlay-active-scrollable');
        $('#nav-search-field').addClass('over-all active');
        $('.modal-closer').removeClass('hidden');
        $('.c-nav-search-container').find('label').removeClass('hidden');
      });

      $('.modal-closer').click(function (e) {
        e.preventDefault();
        $('html').removeClass('overlay-active-scrollable');
        $('#nav-search-field').removeClass('over-all active');
        $('.modal-closer').addClass('hidden');
        $('.c-nav-search-container').find('label').addClass('hidden');
      });

      $web.find('.js-default-menu, .header-menu')
        .on('mouseenter', function () {
          $(this).closest('.nav-header-menu').addClass('open');
          $('.header-menu').removeClass('hidden');
          $('html').addClass('overlay-active');
        })
        .on('mouseleave', function () {
          $(this).closest('.nav-header-menu').removeClass('open');
          $('.header-menu').addClass('hidden');
          $('html').removeClass('overlay-active');
        });

      $web.find('.c-dropdown-opener').on('mouseenter', function () {
        $(this).closest('.js-dropdown-parent').addClass('open');
        $(this).closest('.js-dropdown-parent').children('.js-dropdown').removeClass('hidden');
        $('html').addClass('overlay-active');
      }).on('mouseleave', function () {
        $(this).closest('.js-dropdown-parent').removeClass('open');
        $(this).closest('.js-dropdown-parent').children('.js-dropdown').addClass('hidden');
      });

      $web.find('.js-dropdown')
        .on('mouseenter', function () {
          $(this).closest('.js-dropdown-parent').addClass('open');
          $(this).removeClass('hidden');
        })
        .on('mouseleave', function () {
          $(this).closest('.js-dropdown-parent').removeClass('open');
          $(this).addClass('hidden');
        });

      $('.phone').find('.c-dropdown-opener').on('click', function () {
        var dropdownParent = $(this).closest('.js-dropdown-parent');
        var dropdown = dropdownParent.children('.js-dropdown');
        if ($(this).closest('.js-dropdown-parent').hasClass('js-active-dropdown')) {
          if (dropdownParent.hasClass('open')) {
            dropdownParent.removeClass('open');
            dropdownParent.children('.js-dropdown').addClass('hidden').removeClass('active-submenu');
            $('.js-submenu').addClass('hidden');
            $('html').removeClass('overlay-active');
          } else {
            dropdownParent.addClass('open');
            dropdownParent.children('.js-dropdown').removeClass('hidden');
            $('html').addClass('overlay-active');
          }
        }
      });

      $('.js-dropdown-submenu-opener').on('click', function () {
        $('.js-phone-sidebar').parent('.js-dropdown').addClass('active-submenu');
        var selectedSubmenu = $(this).data('js-submenu');
        $('#' + selectedSubmenu).removeClass('hidden');
      });

      $('.js-back-to-main-dropdown').on('click', function () {
        $('.active-submenu').removeClass('active-submenu');
        $('.js-dropdown-submenu-opener').parent('.js-dropdown-parent').removeClass('open');
        $('.js-submenu').addClass('hidden');
      });

      function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
      }

      if (getParameterByName('q') !== null) {
        $('.header-search-field').val(getParameterByName('q'));
      }

      $.each($('.c-gradient').find('div'), function (index, value) {
        $(value).css('background-position', -($(value).offset().left - $(value).parents('ul').offset().left) + 'px 0');
      });

      if ($body.hasClass('phone')) {
        var $section = $('.c-viewport-height');
        $section.outerHeight($(window).outerHeight() - $('.js-top-nav').outerHeight());
        $(window).resize(function () {
          $section.outerHeight($(window).outerHeight() - $('.js-top-nav').outerHeight());
        });
      }
    $('p:empty').remove();
    }
  );
  $('#products-top-container').find('.product').wrapAll('<div class="main-container" id="products"></div>');
})(jQuery);


(function ($, exports) {
  exports.openDeepLink = openDeepLink;

  var iosMarket = 'https://itunes.apple.com/us/app/apple-store/id587366035?pt=1865588&ct=website&mt=8';
  var androidMarket = 'https://play.google.com/store/apps/details?id=com.picsart.studio&feature=search_result&referrer=utm_source%3Dwebsite%26utm_medium%3Dwebsite%26analytics_source%3Dwebsite%26utm_campaign%3Dwebsite';
  var windowsMarket = 'https://apps.microsoft.com/windows/en-us/app/109e8ce6-65e9-4f52-b1b6-3942a9851cd8';

  setupDeeplink();
  hideTopBannerInSafari();

  function openDeepLink(uri) {
    var browser = (navigator.userAgent || '').toLowerCase();

    uri = uri || $('#deeplink-url').val() || 'picsart://explore';

    if (/android/.test(browser) ||
      /iphone|ipad|ipod/.test(browser)) {
      deeplink.open(uri);
    } else if (/windows phone/.test(browser)) {
      window.location.href = windowsMarket;
    } else {
      UIkit.notify('Sorry, our App is not supported on your device.', {status: 'warning'});
    }
  }

  function setupDeeplink() {
    var browser = (navigator.userAgent || '').toLowerCase();
    if (/android/.test(browser) ||
      /iphone|ipad|ipod/.test(browser) ||
      /windows phone/.test(browser)) {
      //counting all mobile openings
      if (typeof _gaq !== 'undefined') {
        _gaq.push(['_trackEvent', 'Button', 'Click', 'Mobile Open']);
      }
    } else {
      $('#get-app-header').addClass('uk-hidden');
      $('.land-get-app-button').addClass('uk-invisible');
    }

    deeplink.setup({
      iOS: {
        appId: '587366035',
        appName: 'picsart',
        storeUrl: iosMarket
      },
      android: {
        appId: 'com.picsart.studio',
        storeUrl: androidMarket
      }
    });
  }

  function hideTopBannerInSafari() { // TODO change to smart banner
    var userAgent = navigator.userAgent.toLowerCase();
    if (userAgent.indexOf('safari') != -1) {
      if (userAgent.indexOf('chrome') > -1) {
        return; // Chrome
      } else {
        //Automatically close the download bar
        $('.c-close-get-app').click();
      }
    }
  }

  $(document).on('ready', function () {
    //'get the app' case
    if (window.deeplink_open) {
      openDeepLink();
    }
  });

  $(document).on('click', '.js-app', function (e) {
    e.preventDefault();

    openDeepLink();

    return false;
  });
})(jQuery, this);
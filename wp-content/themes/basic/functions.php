<?php

include 'includes/functions-metabox.php';
include 'includes/functions-widget.php';
include 'includes/functions-custom.php';
include 'includes/shortcodes.php';


/* ==========================================================================
 *  Theme settings
 * ========================================================================== */
if (!function_exists('basic_setup')) :
	function basic_setup()
	{

		add_theme_support('post-thumbnails');
		add_theme_support('title-tag');
		add_theme_support('html5', array('search-form', 'gallery', 'caption'));

		register_nav_menus(array(
			'bottom_left' => __('Footer Left Menu', 'basic'),
			'bottom_center' => __('Footer Center Menu', 'basic'),
			'bottom_right' => __('Footer Right Menu', 'basic'),
		));

	}
endif;
add_action('after_setup_theme', 'basic_setup');
/* ========================================================================== */

/* ==========================================================================
 *  Load scripts and styles
 * ========================================================================== */
if (!function_exists('basic_enqueue_style_and_script')) :
	function basic_enqueue_style_and_script()
	{

		// STYLES
		wp_enqueue_style('fontawesome-style', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0');
		wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '4.7.0');
		wp_enqueue_style('main-styling', get_template_directory_uri() . '/css/main.css', array(), '2.4.4');
		wp_enqueue_style('intl-tel-input-css', get_template_directory_uri() . '/css/intlTelInput.css', array(), '11.1.1');

		// SCRIPTS
		if (wp_is_mobile()) {
			wp_enqueue_script('browser-deeplink', get_template_directory_uri() . '/js/browser-deeplink.min.js', array(), '3.8', true);
			wp_enqueue_script('deeplink', get_template_directory_uri() . '/js/deeplink.min.js', array(), '3.8.1', true);
		} else {
			wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.7.3', true);
		}
		wp_enqueue_script('flexmenu-js', get_template_directory_uri() . '/js/flexmenu.min.js', array('jquery'), '1.6.4', true);
		wp_enqueue_script('intel-tel-input-js', get_template_directory_uri() . '/js/intlTelInput.min.js', array('jquery'), '11.1.1', true);
		wp_enqueue_script('basic-scripts', get_template_directory_uri() . '/js/functions.min.js', array('jquery'), '1.7.5', true);
	}
endif;
add_action('wp_enqueue_scripts', 'basic_enqueue_style_and_script');

if (!function_exists('basic_widgets_init')) :
	function basic_widgets_init()
	{
		//Register Clipart Widget Areas
		register_sidebar(array(
			'name' => __('Clipart Front Middle Part', 'basic'),
			'id' => 'front_middle',
			'description' => __('Add widgets here to appear in the middle of the clipart front page.', 'basic'),
		));

		register_sidebar(array(
			'name' => __('Clipart Front Bottom Part', 'basic'),
			'id' => 'front_bottom',
			'description' => __('Add widgets here to appear in the bottom of the clipart front page.', 'basic'),
		));

		// Get App Area Widget Areas
		register_sidebar(array(
			'name' => __('Clipart Front Get App Area', 'basic'),
			'id' => 'get_app_area',
			'description' => __('Add widgets here to appear in the get app area of the clipart front page.', 'basic'),
		));

		register_sidebar(array(
			'name' => __('Collage Front Get App Area', 'basic'),
			'id' => 'collage_get_app_area',
			'description' => __('Add widgets here to appear in the get app area of the collage front page.', 'basic'),
		));

		// Collage Widgets Areas
		register_sidebar(array(
			'name' => __('Collage Front Middle Part', 'basic'),
			'id' => 'collage_front_middle',
			'description' => __('Add widget here to appear in the middle of the collage front page')
		));

		// Backgrounds Widget Areas
		register_sidebar(array(
			'name' => __('Backgrounds Front Middle Part', 'basic'),
			'id' => 'backgrounds_front_middle',
			'description' => __('Add widget here to appear in the middle of the backgrounds front page')
		));

	}
endif;
add_action('widgets_init', 'basic_widgets_init');

//Add Widget edit options to Editor role
$role = get_role('editor');
$role->add_cap('edit_theme_options');

//Function for disabling Wordpress emoji icons
function disable_wp_emojicons()
{
	// all actions related to emojis
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	remove_filter('the_content_feed', 'wp_staticize_emoji');
	remove_filter('comment_text_rss', 'wp_staticize_emoji');
}

add_action('init', 'disable_wp_emojicons');

//Function for removing jQuery migrate script
add_filter('wp_default_scripts', 'dequeue_jquery_migrate');

function dequeue_jquery_migrate(&$scripts)
{
	if (!is_admin()) {
		$scripts->remove('jquery');
		$scripts->add('jquery', false, array('jquery-core'), '1.10.2');
	}
}

//Function for removing wp-embed script
function my_deregister_scripts()
{
	wp_deregister_script('wp-embed');
}

add_action('wp_footer', 'my_deregister_scripts');

add_filter('http_request_args', function ($response, $url) {
	if (0 === strpos($url, 'https://api.wordpress.org/themes/update-check')) {
		$themes = json_decode($response['body']['themes']);
		unset($themes->themes->{get_option('template')});
		unset($themes->themes->{get_option('stylesheet')});
		$response['body']['themes'] = json_encode($themes);
	}
	return $response;
}, 10, 2);

remove_action('wp_head', '_wp_render_title_tag', 1);

function pa_new_search_query($allowed_query_vars)
{
	$allowed_query_vars[] = 'q';
	return $allowed_query_vars;
}

add_filter('query_vars', 'pa_new_search_query');

function pa_modify_post_permalink( $permalink, $post, $leavename)
{
	if (has_category("clipart"))
	{
		$permalink = trailingslashit(home_url('/clipart/'.$post->post_name.'/'));
	}
	return $permalink;
}
add_filter('post_link', 'pa_modify_post_permalink', 10, 3);
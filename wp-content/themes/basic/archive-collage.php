<?php get_header(); ?>
	<main class="content main-page collage-main">
		<?php echo get_featured_block('collage'); ?>
		<div class="main-container">

			<div class="collage-about js-about main-page-content">
				<?php
				if (is_active_sidebar('collage_front_middle')) {
					dynamic_sidebar('collage_front_middle');
				}
				?>
				<div class="frames-block">
					<?php echo get_front_frames_block(); ?>
				</div>
			</div>

			<div class="frontpage-sidebar">
				<?php echo get_collage_front_sidebar(); ?>
			</div>
		</div>
		<div class="clearfix"></div>
		<?php
		if (is_active_sidebar('collage_get_app_area')) {
			dynamic_sidebar('collage_get_app_area');
		}
		?>
	</main>

<?php include 'includes/modal-top.php' ?>

<?php get_footer(); ?>
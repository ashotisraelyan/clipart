<?php
header('Content-Type: application/json');

$args = array(
	'offset' => 0,
	'posts_per_page' => -1,
	'orderby' => 'date',
	'category' => 3,
	'order' => 'DESC',
	'post_type' => 'post',
	'post_status' => 'publish',
	'suppress_filters' => true,
);

$posts = get_posts($args);

$response = array(
	'cliparts' => array()
);

foreach($posts as $post) {
	$deeplink_url = get_post_meta($post->ID, 'Deeplink Url', true);

	$package_uid = substr($deeplink_url, strpos($deeplink_url, 'id=') + 3);

	if($deeplink_url && $deeplink_url != '') {
		$output = array(
			'title' => $post->post_title,
			'link' => get_the_permalink($post->ID),
			'deeplink' => $deeplink_url,
			'package_uid' => $package_uid
		);
		array_push($response['cliparts'], $output);
	}


}
echo json_encode($response);
<?php
/**
 * Template for displaying search form in WordPress pages
 */
?>
<?php
$placeholder = (wp_is_mobile()) ? esc_attr_x('Search...', 'placeholder', 'basic') : esc_attr_x('Search cliparts, collages, backgrounds', 'placeholder', 'basic');
$title = (wp_is_mobile()) ? esc_attr_x('Search...', 'title', 'basic') : esc_attr_x('Search cliparts, collages, backgrounds', 'title', 'basic');
?>
<div class="searchbox-holder">
	<form id="nav-search-field" class="clearfix js-close-searchbox" role="search" method="get" action="<?php echo esc_url(home_url('/')); ?>shop-search/">
		<div class="header-search c-nav-search-container">
			<a href="#" class="modal-closer js-close-searchbox hidden">
				<svg width="17px" height="17px" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<g id="page-6" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g id="close-search-popup" transform="translate(-1460.000000, -28.000000)" fill="#888888">
							<polygon id="close-icon-svg" points="1469.91667 36.5 1477 29.4166667 1475.58333 28 1468.5 35.0833333 1461.41667 28 1460 29.4166667 1467.08333 36.5 1460 43.5833333 1461.41667 45 1468.5 37.9166667 1475.58333 45 1477 43.5833333"></polygon>
						</g>
					</g>
				</svg>
			</a>
			<?php if (!wp_is_mobile()): ?>
				<label for="search-input" class="hidden">Type and press "Enter" to search</label>
			<?php endif; ?>
			<div class="search-input-container position-relative clearfix">
				<input class="float-left header-search-field c-search-input c-phone-header-search" id="search-input" type="search" name="q"
							 placeholder="<?php echo $placeholder; ?>" value=""
							 title="<?php echo $title; ?>" autocomplete="off">
				<div class="search-right-hand js-search-icon">
					<button type="submit" class="search-submit">
						<svg width="18px" class="input-search-icon icon-search" height="18px" viewBox="0 0 18 18" version="1.1"
								 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<g id="page-3" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<g id="search-icon-svg" transform="translate(-1312.000000, -178.000000)" fill="#323232">
									<g id="search-icon-part-2" transform="translate(0.000000, 157.000000)">
										<g id="search-icon-part-1" transform="translate(655.000000, 10.000000)">
											<g id="search-button-svg" transform="translate(657.000000, 11.000000)">
												<path
													d="M0,7.5 C0,11.6355 3.36453505,15 7.50007813,15 C9.02634402,15 10.4461088,14.5395 11.6318712,13.75275 L15.4396608,17.5605 C15.7329139,17.85375 16.1161679,18 16.5001719,18 C16.8841759,18 17.2674299,17.85375 17.5606829,17.5605 C18.146439,16.97475 18.146439,16.02525 17.5606829,15.4395 L13.7528933,11.63175 C14.5396515,10.446 15.0001563,9.02625 15.0001563,7.5 C15.0001563,3.3645 11.6356212,0 7.50007813,0 C3.36453505,0 0,3.3645 0,7.5 L0,7.5 Z M1.50001563,7.5 C1.50001563,4.19175 4.19179366,1.5 7.50007813,1.5 C10.8083626,1.5 13.5001406,4.19175 13.5001406,7.5 C13.5001406,10.80825 10.8083626,13.5 7.50007813,13.5 C4.19179366,13.5 1.50001563,10.80825 1.50001563,7.5 L1.50001563,7.5 Z"
													id="search-Icon"></path>
											</g>
										</g>
									</g>
								</g>
							</g>
						</svg>
					</button>
				</div>
			</div>
		</div>
	</form>
</div>

<?php
/*
  Plugin Name: Picsart Custom Sitemap
  Description: Plugin for creating custom sitemap
  Version: 2.0
  Author: PicsArt
  Author URI: https://picsart.com
*/
register_activation_hook(__FILE__, 'create_picsart_custom_sitemap');
add_action('save_post', 'picsart_sitemaps_init');
function create_picsart_custom_sitemap(){
  if(!file_exists(ABSPATH.'wp-sitemap/clipart/sitemap-clipart.xml') || !file_exists(ABSPATH.'wp-sitemap/collage/sitemap-collage.xml') || !file_exists(ABSPATH.'wp-sitemap/sitemap.xml')){
    die('Please make sure that sitemap.xml files exist in wp-sitemap, wp-sitemap/clipart and wp-sitemap/collage directories');
  }
}
function picsart_sitemaps_init() {
  $output = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.get_site_url().'/wp-content/plugins/picsart-sitemap/sitemap.xsl"?><!-- sitemap-generator-url="http://www.arnebrachhold.de" sitemap-generator-version="4.0.8" -->
<!-- generated-on="February 7, 2017 6:51 am" -->'.PHP_EOL;
  $output .= '<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.PHP_EOL;
  $output .= '<sitemap>'.PHP_EOL;
  $output .= '<loc>'.get_site_url().'/wp-sitemap/clipart/sitemap-clipart.xml</loc>'.PHP_EOL;
  $output .= '<lastmod>'.date('Y-m-d').'</lastmod>'.PHP_EOL;
  $output .= '</sitemap>'.PHP_EOL;
  $output .= '<sitemap>'.PHP_EOL;
  $output .= '<loc>'.get_site_url().'/wp-sitemap/collage/sitemap-collage.xml</loc>'.PHP_EOL;
  $output .= '<lastmod>'.date('Y-m-d').'</lastmod>'.PHP_EOL;
  $output .= '</sitemap>'.PHP_EOL;
  $output .= '<sitemap>'.PHP_EOL;
  $output .= '<loc>'.get_site_url().'/wp-sitemap/backgrounds/sitemap-backgrounds.xml</loc>'.PHP_EOL;
  $output .= '<lastmod>'.date('Y-m-d').'</lastmod>'.PHP_EOL;
  $output .= '</sitemap>'.PHP_EOL;
  $output .= '</sitemapindex>'.PHP_EOL;

  file_put_contents(ABSPATH . 'wp-sitemap/sitemap.xml', $output);
  if (!file_put_contents(ABSPATH . 'wp-sitemap/sitemap.xml', $output)) {
    die('Not able to generate wp-sitemap/sitemap.xml. It\'s not a writable file. Please  give permission of 0777');
  }

  generate_picsart_custom_sitemaps('clipart');
  generate_picsart_custom_sitemaps('collage');
  generate_picsart_custom_sitemaps('backgrounds');
}

function generate_picsart_custom_sitemaps($site){
	$args = array(
		'offset' => 0,
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'DESC',
		'post_status' => 'publish',
		'suppress_filters' => true,
	);
  if($site == 'clipart') {
		$args['post_type'] = 'post';
		$args['category'] = 3;
  } elseif ($site == 'collage') {
		$args['post_type'] = 'collage';
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'picsart_collage_subtype',
				'field' => 'slug',
				'terms' => 'frames'
			)
		);
  } elseif ($site = 'backgrounds') {
		$args['post_type'] = 'backgrounds';
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'picsart_backgrounds_subtype',
				'field' => 'slug',
				'terms' => 'backgrounds-front'
			)
		);
	}

  $posts = get_posts($args);
  if ($posts) {
    $output = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.get_site_url().'/wp-content/plugins/picsart-sitemap/sitemap.xsl"?><!-- sitemap-generator-url="http://www.arnebrachhold.de" sitemap-generator-version="4.0.8" -->
<!-- generated-on="February 7, 2017 6:51 am" -->'.PHP_EOL;
    $output .= '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.PHP_EOL;
    foreach($posts as $post){
      $post_modified = preg_match('/(20)\w+(-)\w+(-)\w+/', $post->post_modified, $matches);
      $post_modified = $matches[0];
      $changefreq = 'daily';
      if(date('Y-m-d', time()) < date('Y-m-d', strtotime($post_modified)+604800)){
        $changefreq = 'daily';
      } elseif(date('Y-m-d', time()) < date('Y-m-d', strtotime($post_modified)+2592000)){
        $changefreq = 'weekly';
      } elseif(date('Y-m-d', time()) > date('Y-m-d', strtotime($post_modified)+2678400)){
        $changefreq = 'monthly';
      } elseif (date('Y-m-d', time()) > date('Y-m-d', strtotime($post_modified)+31536000)){
        $changefreq = 'yearly';
      }

      $output .= '<url>'.PHP_EOL;
      $output .= '<loc>'.get_permalink($post->ID).'</loc>'.PHP_EOL;
      $output .= '<lastmod>'.get_the_date('Y-m-d', $post->ID).'</lastmod>'.PHP_EOL;
      $output .= '<changefreq>'.$changefreq.'</changefreq>'.PHP_EOL;
      $output .= '<priority>0.8</priority>'.PHP_EOL;
      $output .= '</url>'.PHP_EOL;
    }
    $output .= '</urlset>'.PHP_EOL;
    if($site == 'clipart') {
      file_put_contents(ABSPATH . 'wp-sitemap/clipart/sitemap-clipart.xml', $output);
      if (!file_put_contents(ABSPATH . 'wp-sitemap/clipart/sitemap-clipart.xml', $output)) {
        die('Not able to generate wp-sitemap/clipart/sitemap-clipart.xml. It\'s not a writable file. Please  give permission of 0777');
      }

    } elseif ($site == 'collage'){
        file_put_contents(ABSPATH.'wp-sitemap/collage/sitemap-collage.xml', $output);
        if(!file_put_contents(ABSPATH.'wp-sitemap/collage/sitemap-collage.xml', $output)){
          die('Not able to generate wp-sitemap/collage/sitemap-collage.xml. It\'s not a writable file. Please  give permission of 0777');
        }
      }
      elseif ($site == 'backgrounds'){
        file_put_contents(ABSPATH.'wp-sitemap/backgrounds/sitemap-backgrounds.xml', $output);
        if(!file_put_contents(ABSPATH.'wp-sitemap/backgrounds/sitemap-backgrounds.xml', $output)){
          die('Not able to generate wp-sitemap/backgrounds/sitemap-backgrounds.xml. It\'s not a writable file. Please  give permission of 0777');
        }
      }
    wp_remote_get("http://www.google.com/webmasters/sitemaps/ping?sitemap=".get_site_url().'wp-sitemap/sitemap.xml');
    wp_remote_get("http://www.bing.com/ping?siteMap=".get_site_url().'wp-sitemap/sitemap.xml');
  }
}

function not_writable_sitemap_notice() {
  ?>
  <div class="notice notice-error is-dismissible">
    <p><?php _e( 'Not able to generate sitemap. Please check file permissions!', 'basic' ); ?></p>
  </div>
  <?php
}

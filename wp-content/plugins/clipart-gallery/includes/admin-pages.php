<?php
function gallery_admin_page() {
    global $site_url;
    if (!current_user_can('manage_categories')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    ?>
    <div class="wrap gallery">
        <h1>Galleries
            <span class="add_new"><a href="<?php echo $site_url;?>/wp-admin/admin.php?page=new_gallery" class="page-title-action">Add New</a></span>
        </h1>
    </div>
    <?php
    backend_form_handler();
    get_backend_galleries();
}

function gallery_add_page() {
    global $site_url;
    if (!current_user_can('manage_categories')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    ?>
    <div class="wrap add_gallery gallery">
        <h1>Add New Gallery</h1>
        <form action="<?php echo $site_url;?>/wp-admin/admin.php?page=gallery" class="gallery_form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title">Gallery Title</label>
                <input type="text" name="title" placeholder="Title" id="title" class="form-control title_field" />
            </div>
            <div class="form-group uploads">
                <label for="image">Add images</label>
                <input type="file" name="image[]" id="image" class="upload_field" accept=".jpg, .png" multiple>
            </div>
            <?php submit_button('Post') ?>
        </form>
    </div>
    <?php
}

function get_backend_galleries() {
    global $wpdb;
    global $site_url;

    $all_data = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "clipart_gallery ORDER BY id DESC");
    if (!$all_data) {
        echo '<div class="alert alert-danger gallery_alert">' .
            '<strong>No</strong> result.' .
            '<span class="message_close_btn dashicons dashicons-no"></span>' .
            '</div>';
        return;
    }
    ?>
    <div class="wrap gallery">
        <div class="table-responsive">
            <table class="table table-bordered table-hover gallery_table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Gallery ID</th>
                    <th>Name</th>
                    <th>Author</th>
                    <th>Image Count</th>
                    <th>Udpate</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($all_data as $key => $data) { ?>
                    <tr>
                        <td> <?php echo $key + 1 . '.'; ?> </td>
                        <td><?php echo $data->id; ?></td>
                        <td> <?php echo $data->name; ?> </td>
                        <td> <?php echo get_user_by('id', $data->user_id)->display_name; ?> </td>
                        <td> <?php get_gallery_image_count($data->id); ?> </td>
                        <td class="update">
                            <a class="update_icon" href="<?php echo $site_url;?>/wp-admin/admin.php?page=update_gallery&id=<?php echo $data->id ?>"><span class="dashicons dashicons-edit"></span></a>
                        </td>
                        <td class="delete">
                            <span class="delete_icon" href="#" data-gallery-url="<?php echo $data->path; ?>" data-gallery-id="<?php echo $data->id; ?>"><span class="dashicons dashicons-trash"></span></span>
                        </td>
                    </tr> <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
}

function gallery_update_page() {
    global $wpdb;
    global $site_url;
    if (!current_user_can('manage_categories')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    ?>
    <div class="wrap gallery update_gallery">
        <h1>Update Gallery</h1>
        <?php
        if (!isset($_GET['id']) && !(int) $_GET['id']) {
            return;
        }
        updated_form_handler();

        $gallery_id = (int) $_GET['id'];

        $gallery_row = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'clipart_gallery WHERE id = %d', $gallery_id));

        $gallery_images = $wpdb->get_results($wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'clipart_gallery_images WHERE gallery_id = %d', $gallery_id));

        ?>

        <form class="gallery_form" method="post" enctype="multipart/form-data">
            <div class="form-group title">
                <label for="title" class="title_label">Gallery Title</label>
                <input type="text" id="title" name="title" class="form-control" value="<?php echo $gallery_row->name; ?>" />
            </div>
            <div class="images">
                <?php foreach ($gallery_images as $image) { ?>
                    <div class="form-group single_image">
                        <div class="image_cont">
                            <img class="gallery_image" src="<?php echo $site_url.'/'.$gallery_row->path.'/'.$image->filename; ?>"/>
                        </div>
                        <span class="image_delete_icon" href="#" gallery-image-url="<?php echo $gallery_row->path.'/'.$image->filename; ?>" gallery-image-id="<?php echo $image->id; ?>"><span class="dashicons dashicons-trash"></span></span>
                    </div>
                    <?php
                }
                ?>
                <div class="form-group uploads">
                    <label for="image">Add images</label>
                    <input type="file" name="image[]" id="image" class="upload_field" accept=".jpg, .png" multiple>
                </div>
            </div>
            <input type="hidden" name="gallery_path" value="<?php echo $gallery_row->path; ?>">
            <?php submit_button('Save') ?>
        </form>
    </div>
    <?php
}

function gallery_options_page() {
    if (!current_user_can('manage_categories')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    ?>
    <div class="wrap gallery">
        <h1>Gallery options</h1>
        <form action='options.php' method='post'>
            <?php
            settings_fields( 'clipart_gallery_settings' );
            do_settings_sections( 'clipart_gallery_settings' );
            submit_button();
            ?>
        </form>
    </div>
    <?php
}

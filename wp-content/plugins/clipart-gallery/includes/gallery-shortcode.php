<?php

function clipart_gallery_id($atts) {
  global $wpdb;
  global $site_url;
  $atts = shortcode_atts(
    array(
      'id' => '',
      'background_color' => '#fff',
      'quantity' => '6'
    ), $atts
  );

  $gallery_id = $atts['id'];
  $gallery_background = $atts['background_color'];

  if($gallery_id){
    $gallery_row = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'clipart_gallery WHERE id = %d', $gallery_id));
    $images = $wpdb->get_results($wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'clipart_gallery_images WHERE gallery_id = %d', $gallery_id));

    if($gallery_row && $images){
      $output = '<div class="post_gallery" style="background-color: '.$gallery_background.';">';
      $output .='<div class="gallery_container main-container">';
      $output .= '<ul class="image_list">';
      foreach($images as $image){
        if($atts['quantity'] == '6'){
          $output .= '<li itemscope itemtype="http://schema.org/ImageObject" class="uk-width-1-6">';
        } elseif($atts['quantity'] == '8'){
          $output .= '<li itemscope itemtype="http://schema.org/ImageObject" class="uk-width-1-8">';
        } elseif($atts['quantity'] == '5'){
          $output .= '<li itemscope itemtype="http://schema.org/ImageObject" class="uk-width-1-5">';
        } elseif($atts['quantity'] == '4'){
          $output .= '<li itemscope itemtype="http://schema.org/ImageObject" class="uk-width-1-4">';
        }
        $output .= '<img itemprop="contentUrl" src="'.$site_url.'/'.$gallery_row->path . '/' . $image->filename .'" />';
        $output .= '</li>';
      }
      $output .= '</ul>';

      if(wp_is_mobile()){
        $get_app_class = '';
        if(get_post_meta(get_the_ID(), 'Deeplink Url', true)){
          $deeplink_url = get_post_meta(get_the_ID(), 'Deeplink Url', true);
        } else{
          $deeplink_url = 'picsart://explore';
        }
      } else{
        $get_app_class = 'js-get-app-modal-bottom';
        $deeplink_url = '#';
      }

      $output .= '<a href="'.$deeplink_url.'" data-js-ga-click="clipart get-clipart source-bottom-section" class="c-ga-click get-package '.$get_app_class.'">Get clipart</a>';
      $output .= '<a href="/clipart" class="back-to-category-link">more clipart collections</a>';
      $output .= '</div>';
      $output .= '</div>';

      return $output;
    }
  }
}

add_shortcode('clipart_gallery', 'clipart_gallery_id');

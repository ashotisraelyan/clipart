<?php
include('includes/admin-pages.php');
include('includes/gallery-shortcode.php');

function backend_form_handler() {
  if (isset($_POST["submit"])) {
    global $wpdb;

    $user_id = get_current_user_id();

    if(isset($_POST['title']) && $_POST['title']){
      $title = $_POST['title'];
    }
    if(isset($_FILES['image']) && $_FILES['image']){
      $files = $_FILES['image'];
    }

    $random_name = generateRandomString().'_'.date("H_i", time());

    $path = 'wp-content/clipart-gallery/'.$random_name;

    $move_path = get_home_path().$path;

    if($files){
      wp_mkdir_p($move_path);
    }

    $wpdb->query($wpdb->prepare(
    'INSERT INTO ' . $wpdb->prefix . 'clipart_gallery ( name, path, user_id ) VALUES ( %s, %s, %d )', $title, $path, $user_id
    ));

    $gallery_id = $wpdb->insert_id;

    uploaded_file_handler($files, $move_path, $gallery_id);
  }
}

add_action('wp_ajax_delete_gallery', 'delete_gallery');

function delete_gallery() {
  if (isset($_POST['delete']) && (int) $_POST['delete']) {
    global $wpdb;

    $gallery_id = (int) $_POST['delete'];

    delete_directory(get_home_path().$_POST['url']);

    $delete_gallery = $wpdb->delete($wpdb->prefix . 'clipart_gallery', array('ID' => $gallery_id));
    $delete_gallery_images = $wpdb->delete($wpdb->prefix . 'clipart_gallery_images', array('gallery_id' => $gallery_id));

    if ($delete_gallery) {
      $response = array('status' => 'ok', 'message' => '');
    } else {
      $response = array('status' => 'error', 'message' => 'Error!!! Poll wasn\'t deleted. Try again.');
    }

    return wp_send_json($response);
    }
}

add_action('wp_ajax_delete_image', 'delete_image');

function delete_image() {
  if (isset($_POST['delete']) && (int) $_POST['delete']) {
    global $wpdb;

    $image_id = (int) $_POST['delete'];

    unlink(get_home_path().$_POST['url']);

    $delete_gallery_image = $wpdb->delete($wpdb->prefix . 'clipart_gallery_images', array('id' => $image_id));

    if ($delete_gallery_image) {
      $response = array('status' => 'ok', 'message' => '');
    } else {
      $response = array('status' => 'error', 'message' => 'Error!!! Poll wasn\'t deleted. Try again.');
    }
    return wp_send_json($response);
    }
}

function updated_form_handler() {
  global $wpdb;
  global $site_url;

  if (isset($_POST["submit"])) {
    $user_id = get_current_user_id();
    $gallery_id = (int) $_GET['id'];
    if (isset($_POST['title']) && $_POST['title']) {
      $title = $_POST['title'];
    }
    if (isset($_POST['gallery_path']) && $_POST['gallery_path']) {
      $path = $_POST['gallery_path'];
    }
    if(isset($_FILES['image']) && $_FILES['image']){
      $files = $_FILES['image'];
    }

    $gallery_name_update = $wpdb->update( $wpdb->prefix . 'clipart_gallery', array(
        'name' => $title,
        'user_id' => $user_id,
      ), array('ID' => $gallery_id), array(
        '%s', '%d'
      )
    );

    $move_path = get_home_path().$path;

    uploaded_file_handler($files, $move_path, $gallery_id);

    if($gallery_name_update){
      $output = '<div class="alert alert-success gallery_alert">';
      $output .= '<strong>Success!</strong> Gallery was updated';
      $output .= '<span class="message_close_btn dashicons dashicons-no"></span>';
      $output .= '</div>';
      $output .= '<a href="'.$site_url.'/wp-admin/admin.php?page=gallery" class="return">Return to Galleries</a>';

      echo $output;
    }
  }
}

function uploaded_file_handler($files, $move_path, $gallery_id) {
  global $wpdb;
  foreach ($files["error"] as $key => $error) {
    if ($error == UPLOAD_ERR_OK) {

      $tmp_name = $files["tmp_name"][$key];
      $name = $files["name"][$key];

      move_uploaded_file($tmp_name, "$move_path/$name");

      $wpdb->query($wpdb->prepare(
      'INSERT INTO ' . $wpdb->prefix . 'clipart_gallery_images ( gallery_id, filename ) VALUES ( %d, %s )', $gallery_id, $name
      ));
    }
  }
}

function get_gallery_image_count($gallery_id) {
  global $wpdb;

  $image_count = $wpdb->get_var($wpdb->prepare('SELECT SUM(gallery_id) FROM ' . $wpdb->prefix . 'clipart_gallery_images WHERE gallery_id = %d', $gallery_id));

  echo $image_count/$gallery_id;
}

function generateRandomString($length = 10) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}

function delete_directory($dir) {
  $files = array_diff(scandir($dir), array('.','..'));
  foreach ($files as $file) {
    (is_dir("$dir/$file")) ? delete_directory("$dir/$file") : unlink("$dir/$file");
  }
  return rmdir($dir);
}

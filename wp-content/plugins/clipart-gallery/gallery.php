<?php

/*
  Plugin Name: Clipart Gallery
  Description: Plugin for creating galleries
  Version: 2.3
  Author: Ashot Israelyan
  Author URI: https://www.facebook.com/ashot.israelyan
 */

include('functions.php');

global $site_url;
$site_url = get_site_url();

add_action('admin_menu', 'register_gallery_index');
add_action('admin_enqueue_scripts', 'gallery_admin_scripts_init');

register_activation_hook(__FILE__, 'create_gallery_db_tables');
register_activation_hook(__FILE__, 'create_gallery_directory');

function register_gallery_index() {
  add_menu_page('Galleries', 'Galleries', 'manage_categories', 'gallery', 'gallery_admin_page', 'dashicons-format-gallery', 30);
  add_submenu_page('gallery', 'Add New Gallery', 'Add New Gallery', 'manage_categories', 'new_gallery', 'gallery_add_page');
  add_submenu_page('update_gallery', 'Update Gallery', null, 'manage_categories', 'update_gallery', 'gallery_update_page');
}

function gallery_admin_scripts_init($hook_suffix) {
  if ($hook_suffix == 'galleries_page_new_gallery' || $hook_suffix == 'toplevel_page_gallery' || $hook_suffix == 'admin_page_update_gallery') {
      $pluginfolder = get_bloginfo('url') . '/' . PLUGINDIR . '/' . dirname(plugin_basename(__FILE__));
      wp_enqueue_style('bootstrap', $pluginfolder . '/css/bootstrap.min.css');
      wp_enqueue_style('bootstrap-theme', $pluginfolder . '/css/bootstrap-theme.min.css');
      wp_enqueue_style('gallery-plugin-style', $pluginfolder . '/css/style.css');
      wp_enqueue_script('bootstrap', $pluginfolder . '/js/bootstrap.min.js', array('jquery'), null, true);
      wp_enqueue_script('bootbox', $pluginfolder . '/js/bootbox.min.js', array('jquery'), null, true);
      wp_enqueue_script('gallery-backend-js', $pluginfolder . '/js/backend.js', array('jquery'), null, true);
  }
}

function create_gallery_db_tables() {
    global $wpdb;
    if ($wpdb->get_var("show tables like '" . $wpdb->prefix . "clipart_gallery'") != $wpdb->prefix . 'clipart_gallery') {

      $query = "CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "clipart_gallery` ("
              . "`id` INT NOT NULL AUTO_INCREMENT ,"
              . "`name` varchar(255) CHARACTER SET utf8 default '0',"
              . "`path` varchar(255) CHARACTER SET utf8 default '0',"
              . "`user_id` INT NOT NULL default '0' ,"
              . "PRIMARY KEY ( `id` )"
              . ")";
        $wpdb->query($query);
    }

    if ($wpdb->get_var("show tables like '" . $wpdb->prefix . "clipart_gallery_images'") != $wpdb->prefix . 'clipart_gallery_images') {

      $query = "CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "clipart_gallery_images` ("
              . "`id` INT NOT NULL AUTO_INCREMENT ,"
              . "`gallery_id` INT NOT NULL default '0' ,"
              . "`filename` varchar(255) CHARACTER SET utf8 default '0',"
              . "PRIMARY KEY ( `id` )"
              . ")";
        $wpdb->query($query);
    }
}

function create_gallery_directory(){

  $settings_path = get_option('clipart_settings');

  $path = get_home_path().'wp-content/clipart-gallery';

  if (!file_exists($path)) {
    wp_mkdir_p( $path );
  }
}

?>

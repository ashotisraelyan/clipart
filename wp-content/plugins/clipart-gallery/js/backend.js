(function ($) {
  $(document).ready(function () {
    $('.message_close_btn').click(function () {
      $(this).parent().addClass('closed')
      if ('.return') {
        $('.return').addClass('closed')
      }
    })

    $('.delete_icon').click(function () {
      var current_delete_icon = this
      bootbox.confirm("Are you sure you want to delete the gallery?", function (result) {
        if (result) {
          var data = {
            action: 'delete_gallery',
            delete: parseInt($(current_delete_icon).attr('data-gallery-id')),
            url: $(current_delete_icon).attr('data-gallery-url'),
          }
          $.post('admin-ajax.php', data, function (response) {
            if (response.status == 'ok') {
              bootbox.alert("The gallery was deleted", function(){
                location.reload();
              });
            } else {
              bootbox.alert('The gallery wasn\'t deleted!')
            }
          })
        }
      })
    })

    $('.image_delete_icon').click(function () {
      var image_delete_icon = this
      bootbox.confirm("Are you sure you want to delete this image?", function (result) {
        if (result) {
          var data = {
            action: 'delete_image',
            delete: parseInt($(image_delete_icon).attr('gallery-image-id')),
            url: $(image_delete_icon).attr('gallery-image-url'),
          }
          $.post('admin-ajax.php', data, function (response) {
            if (response.status == 'ok'){
              bootbox.alert("The image was deleted", function(){
                location.reload();
              });
            } else {
              bootbox.alert('The image wasn\'t deleted!');
            }
          })
        }
      })
    })
  })
})(jQuery);

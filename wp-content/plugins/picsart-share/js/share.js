(function ($) {
  $(document).ready(function ($) {
    $('.ajax_loader').hide();
    $('#submit').click(function (e) {
      e.preventDefault();

      var category = $('#select_posts option:selected').val();
      var count = parseInt($('#count').val());

      if (category == '0') {
        alert('Please Select the post type');
        return false;
      }

      if (count > 100) {
        alert('The count should be less than 100');
        return false;
      }

      $('.ajax_loader').show();
      $('body').css('overflow', 'hidden');

      var data = {
        action: 'get_share_posts',
        select_posts: category,
        count: count
      };

      $.post('admin-ajax.php', data)
        .done(function (response) {
          renderShareHTML(response);

          $('.ajax_loader').hide();
          $('body').css('overflow', 'auto');
        })
        .fail(function () {
          $('.ajax_loader').hide();
          $('body').css('overflow', 'auto');
          $('.error-notice').removeClass('hidden');
        });
    });

    function renderShareHTML(posts) {
      var htmlString = '';
      $.each(posts, function (i, post) {
        var number = i + 1;
        htmlString += '<tr class="post js-post" data-url="' + post.url + '">';
        htmlString += '<td class="post_id">' + number + '</td>';
        htmlString += '<td class="post_title">' + post.title + '</td>';
        htmlString += '<td class="post_url"><a href="' + post.url + '">' + post.url + '</a></td>';
        htmlString += '<td class="facebook js-social" data-social-type="facebook"> ... </td>';
        htmlString += '<td class="google js-social" data-social-type="google"> ... </td>';
        htmlString += '<td class="pinterest js-social" data-social-type="pinterest"> ... </td>';
        htmlString += '<td class="reddit js-social" data-social-type="reddit"> ... </td>';
        htmlString += '<td class="linkedin js-social" data-social-type="linkedin"> ... </td>';
        htmlString += '<td class="total js-total" data-social-type="total"> ... </td>';
        htmlString += '</tr>';
      });

      var tableBody = $('#share_table_body');

      tableBody.html(htmlString);

      var countMap = {};
      var loadings = 0;

      tableBody.find('.js-post').each(function () {
        var totalCount = 0;
        $(this).find('.js-social').each(function () {
          var $this = $(this);
          var socialType = $(this).data('social-type');
          var url = $this.closest('.js-post').data('url');

          loadings++;
          getShareStatus(url, socialType)
            .then(function (shareCount) {
              $this.text(shareCount);

              if (!countMap[socialType]) {
                countMap[socialType] = 0;
              }
              if (!countMap['total']) {
                countMap['total'] = 0;
              }

              totalCount += shareCount;

              countMap[socialType] += shareCount;
              countMap['total'] += shareCount;

              $this.siblings('.js-total').text(totalCount);

              if (--loadings === 0) {
                completed();
              }
            })
            .fail(function (err) {
              $('.error-notice').removeClass('hidden');
              $this.css('color', 'red').text('!');

              if (--loadings === 0) {
                completed();
              }
            });
        });
      });

      function completed() {
        var tableFooter = $('#share_table_footer');
        tableFooter.find('.total-facebook').text(countMap['facebook']);
        tableFooter.find('.total-google').text(countMap['google']);
        tableFooter.find('.total-pinterest').text(countMap['pinterest']);
        tableFooter.find('.total-reddit').text(countMap['reddit']);
        tableFooter.find('.total-linkedin').text(countMap['linkedin']);
        tableFooter.find('.total').text(countMap['total']);
      }
    }

    function getShareStatus(link, site) {
      var defer = $.Deferred();

      if (site === 'facebook') {
        var token = '1847204285512657|43713065588e4ce5a4b6b6403232d0ac';

        $.ajax({
          url: 'https://graph.facebook.com/v2.7/',
          dataType: 'jsonp',
          type: 'GET',
          data: {access_token: token, id: link},
          success: function (data) {
            if (data.share.share_count) {
              defer.resolve(data.share.share_count);
            } else {
              defer.resolve(0);
            }
          },
          error: function (err) {
            console.log(err);
            defer.reject(err);
          }
        });
      } else if (site === 'google') {
        $.getJSON('https://count.donreach.com/?url=' + link + '&providers=google')
          .done(function (data) {
            defer.resolve(data.shares.google);
          })
      } else if (site === 'pinterest') {
        var pin = 'https://api.pinterest.com/v1/urls/count.json?callback=?';
        $.getJSON(pin, {
          url: link
        })
          .done(function (data) {
            defer.resolve(data.count);
          })
          .fail(function (err) {
            console.log(err);
            defer.reject(err);
          });
      } else if (site === 'linkedin') {
        $.ajax({
          url: 'https://www.linkedin.com/countserv/count/share?url=' + link,
          dataType: 'jsonp',
          type: 'GET',
          success: function (data) {
            defer.resolve(data.count);
          },
          error: function (err) {
            console.log(err);
            defer.reject(err);
          }
        })
      } else if (site === 'reddit') {
        $.getJSON('https://count.donreach.com/?url=' + link + '&providers=reddit')
          .done(function (data) {
            defer.resolve(data.shares.reddit);
          })
          .fail(function (err) {
            console.log(err);
            defer.reject(err);
          });
      }
      else {
        throw new Error('Unknown social network');
      }
      return defer.promise();
    }

    $('.js-sort').on('click', function () {
      var table = $('.share_table');
      var rows = $('.js-post').toArray().sort(comparer($(this).index()))
      this.asc = !this.asc;
      if (!this.asc) {
        rows = rows.reverse();
      }
      for (var i = 0; i < rows.length; i++) {
        table.append(rows[i]);
      }
    });

    function comparer(index) {
      return function (a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index);
        return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB);
      }
    }

    function getCellValue(row, index) {
      return $(row).children('td').eq(index).html();
    }
  });
})(jQuery);
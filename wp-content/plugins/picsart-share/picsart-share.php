<?php

/*
 Plugin Name: Picsart Share
	Description: Plugin for checking Picsart's Blog's post share count
	Version: 1.0 beta
	Author: Picsart
	Author URI: https://picsart.com
 */

add_action('admin_menu', 'register_picsart_share_menu');
add_action('admin_enqueue_scripts', 'picsart_share_scripts_init');

function register_picsart_share_menu()
{
	add_menu_page('Share Count', 'Share Count', 'manage_categories', 'share_count', 'share_count_admin_page', 'dashicons-share', 30);
}

function picsart_share_scripts_init($hook_suffix)
{
	if ($hook_suffix == 'toplevel_page_share_count') {
		$pluginfolder = '/' . PLUGINDIR . '/' . dirname(plugin_basename(__FILE__));
		wp_enqueue_style('picsart-share-plugin-style', $pluginfolder . '/css/style.css', array(), '2');
		wp_enqueue_script('share-picsart-js', $pluginfolder . '/js/share.js', array('jquery'), '2', true);
	}
}

function share_count_admin_page()
{
	if (!current_user_can('manage_categories')) {
		wp_die(__('You don\'t have sufficient permission to access this page.'));
	}
	?>
	<div class="wrap picsart_share">
		<h1>Picsart Share Counts</h1>
	</div>
	<div class="ajax_loader">
		<div id="floatingBarsG">
			<div class="blockG" id="rotateG_01"></div>
			<div class="blockG" id="rotateG_02"></div>
			<div class="blockG" id="rotateG_03"></div>
			<div class="blockG" id="rotateG_04"></div>
			<div class="blockG" id="rotateG_05"></div>
			<div class="blockG" id="rotateG_06"></div>
			<div class="blockG" id="rotateG_07"></div>
			<div class="blockG" id="rotateG_08"></div>
		</div>
	</div>
	<div class="share_form">
		<div class="col-md-3">
			<label for="select_posts">Select Post Type</label>
			<select id="select_posts" name="select_posts" class="select_category form-control">
				<option value="0">Posts</option>
				<option class="category_options" value="clipart">Clipart</option>
				<option class="category_options" value="collage">Collage</option>
			</select>
		</div>
		<div class="col-md-3">
			<label for="count">Type post count to generate</label>
			<input type="text" name="count" id="count" class="count_input form-control" value="5">
		</div>
		<div class="col-md-3">
			<input type="submit" name="submit" id="submit" class="button button-primary" value="Generate">
		</div>
		<input type="hidden" name="sietUrl" id="siteUrl" class="hidden" value="<?php echo get_site_url(); ?>"
	</div>
	<div class="clearfix"></div>
	<div class="alert alert-danger hidden error-notice">
		<strong>Error!</strong> Something went wrong, please try again later.
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-hover share_table">
			<thead>
			<tr>
				<th>#</th>
				<th>Post Title</th>
				<th>Post Url</th>
				<th data-sort="facebook">Facebook</th>
				<th data-sort="google">Google+</th>
				<th data-sort="pinterest">Pinterest</th>
				<th data-sort="reddit">Reddit</th>
				<th data-sort="likedin">Linkedin</th>
				<th class="js-sort" data-sort="total">Total</th>
			</tr>
			</thead>
			<tbody id="share_table_body"></tbody>
			<tfoot id="share_table_footer">
			<tr>
				<td colspan="3"><strong>Total count</strong></td>
				<td class="total-facebook js-footer-total" data-social-type="facebook"></td>
				<td class="total-google js-footer-total" data-social-type="google"></td>
				<td class="total-pinterest js-footer-total" data-social-type="pinterest"></td>
				<td class="total-reddit js-footer-total" data-social-type="reddit"></td>
				<td class="total-linkedin js-footer-total" data-social-type="likedin"></td>
				<td class="total js-footer-total" data-social-type="total"></td>
			</tr>
			</tfoot>
		</table>
	</div>
	<?php
}

add_action('wp_ajax_get_share_posts', 'get_share_posts');

function get_share_posts()
{
	if (isset($_POST['count'])) {
		$post_type = '';
		$count = '';

		if (isset($_POST['select_posts']) && $_POST['select_posts']) {
			$post_type = $_POST['select_posts'];
		}

		if (isset($_POST['count']) && $_POST['count']) {
			$count = (int)$_POST['count'];
		}

		$args = '';

		if ($post_type == 'clipart') {
			$args = array(
				'offset' => 0,
				'posts_per_page' => $count,
				'orderby' => 'date',
				'order' => 'DESC',
				'post_status' => 'publish',
				'suppress_filters' => true,
				'post_type' => 'post',
				'category' => 3
			);
		} else if ($post_type == 'collage') {
			$args = array(
				'offset' => 0,
				'posts_per_page' => $count,
				'orderby' => 'date',
				'order' => 'DESC',
				'post_type' => 'collage',
				'post_status' => 'publish',
				'suppress_filters' => true,
				'tax_query' => array(
					array(
						'taxonomy' => 'picsart_collage_subtype',
						'field' => 'slug',
						'terms' => 'frames'
					)
				)
			);
		}

		$posts = get_posts($args);
		$response = array();

		foreach ($posts as $post) {
			$permalink = str_replace('http://', 'https://', get_permalink($post->ID));

			$output = array(
				'id' => $post->ID,
				'title' => $post->post_title,
				'url' => $permalink
			);
			array_push($response, $output);
		}
		wp_send_json($response);
	}
}
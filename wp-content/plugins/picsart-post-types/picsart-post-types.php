<?php

/* Plugin Name: Picsart Custom Post Types
   Description: Plugin for creating custom pages
   Version: 1.0
   Author: Ashot Israelyan
   Author URI: https://www.facebook.com/ashot.israelyan
*/

add_action('init', 'register_picsart_custom_post_types');

function register_picsart_custom_post_types()
{
	//Register Banner Custom Post type
	if (!post_type_exists('banner')) {
		register_post_type('banner', array(
				'labels' => array(
					'name' => __('Banners'),
					'singular_name' => __('Banner'),
					'add_new' => __('Add New Banner', 'Banner'),
					'new_item' => __('New Banner'),
					'edit_item' => __('Edit Banner'),
					'view_item' => __('View Banner'),
					'all_items' => __('All Banners'),
					'search_items' => __('Search Banners'),
					'parent_item_colon' => __('Parent Banners'),
					'not_found' => __('No banners found'),
					'not_found_in_trash' => __('No banners found in Trash'),
				),
				'public' => true,
				'show_in_rest' => true,
				'publicly_queryable' => true,
				'exclude_from_search' => false,
				'menu_icon' => 'dashicons-tag',
				'has_archive' => true,
				'rewrite' => array('slug' => 'banner', 'with_front' => false),
				'supports' => array('title', 'editor', 'revisions'),
			)
		);
		flush_rewrite_rules();
	}

	//Register Collage Custom Post Type
	if (!post_type_exists('collage')) {
		register_post_type('collage', array(
				'labels' => array(
					'name' => __('Collage'),
					'singular_name' => __('Collage'),
					'add_new' => __('Add New Collage', 'Collage'),
					'new_item' => __('New Collage'),
					'edit_item' => __('Edit Collage'),
					'view_item' => __('View Collage'),
					'all_items' => __('All Collages'),
					'search_items' => __('Search Collages'),
					'parent_item_colon' => __('Parent Collages'),
					'not_found' => __('No collages found'),
					'not_found_in_trash' => __('No collages found in Trash'),
				),
				'public' => true,
				'publicly_queryable' => true,
				'exclude_from_search' => false,
				'menu_icon' => 'dashicons-admin-customizer',
				'has_archive' => true,
				'rewrite' => array('slug' => 'collage', 'with_front' => false),
				'supports' => array('title', 'editor', 'revisions', 'thumbnail', 'excerpt'),
			)
		);
		flush_rewrite_rules();
	}

	if (!post_type_exists('backgrounds')) {
		register_post_type('backgrounds', array(
				'labels' => array(
					'name' => __('Backgrounds'),
					'singular_name' => __('Background'),
					'add_new' => __('Add New Background', 'Backgrounds'),
					'new_item' => __('New Background'),
					'edit_item' => __('Edit Background'),
					'view_item' => __('View Background'),
					'all_items' => __('All Backgrounds'),
					'search_items' => __('Search Background'),
					'parent_item_colon' => __('Parent Background'),
					'not_found' => __('No background found'),
					'not_found_in_trash' => __('No backgrounds found in Trash'),
				),
				'public' => true,
				'publicly_queryable' => true,
				'exclude_from_search' => false,
				'menu_icon' => 'dashicons-format-image',
				'has_archive' => true,
				'rewrite' => array('slug' => 'backgrounds', 'with_front' => false),
				'supports' => array('title', 'editor', 'revisions', 'thumbnail', 'excerpt'),
			)
		);
		flush_rewrite_rules();
	}
}

// Show posts of 'post', 'page' and 'movie' post types on home page
add_action('pre_get_posts', 'add_my_post_types_to_query');

function add_my_post_types_to_query($query)
{
	if (is_home() && $query->is_main_query())
		$query->set('post_type', array('post', 'page', 'banner', 'collage', 'backgrounds', 'apps', 'features'));
	return $query;
}

function wporg_register_taxonomy_course()
{
	//Register Banner Taxonomy
	if (!taxonomy_exists('picsart_banner_subtype')) {

		$labels = [
			'name' => _x('Banner Subtypes', 'taxonomy general name'),
			'singular_name' => _x('Banner Subtype', 'taxonomy singular name'),
			'search_items' => __('Search Banner Subtypes'),
			'all_items' => __('All Banner Subtypes'),
			'parent_item' => __('Parent Banner Subtype'),
			'parent_item_colon' => __('Parent Banner Subtype:'),
			'edit_item' => __('Edit Banner Subtype'),
			'update_item' => __('Update Banner Subtype'),
			'add_new_item' => __('Add New Banner Subtype'),
			'new_item_name' => __('New Banner Subtype Name'),
			'menu_name' => __('Banner Subtype'),
		];
		$args = [
			'hierarchical' => true, // make it hierarchical (like categories)
			'labels' => $labels,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_quick_edit' => true,
			'query_var' => true,
		];
		register_taxonomy('picsart_banner_subtype', ['banner'], $args);
	}

	//Register Collage Taxonomy
	if (!taxonomy_exists('picsart_collage_subtype')) {

		$labels = [
			'name' => _x('Collage Subtypes', 'taxonomy general name'),
			'singular_name' => _x('Collage Subtype', 'taxonomy singular name'),
			'search_items' => __('Search Collage Subtypes'),
			'all_items' => __('All Collage Subtypes'),
			'parent_item' => __('Parent Collage Subtype'),
			'parent_item_colon' => __('Parent Collage Subtype:'),
			'edit_item' => __('Edit Collage Subtype'),
			'update_item' => __('Update Collage Subtype'),
			'add_new_item' => __('Add New Collage Subtype'),
			'new_item_name' => __('New Collage Subtype Name'),
			'menu_name' => __('Collage Subtype'),
		];
		$args = [
			'public' => true,
			'hierarchical' => true, // make it hierarchical (like categories)
			'labels' => $labels,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_quick_edit' => true,
			'query_var' => true
		];
		register_taxonomy('picsart_collage_subtype', ['collage'], $args);
	}

	if (!taxonomy_exists('picsart_backgrounds_subtype')) {

		$labels = [
			'name' => _x('Backgrounds Subtypes', 'taxonomy general name'),
			'singular_name' => _x('Backgrounds Subtype', 'taxonomy singular name'),
			'search_items' => __('Search Backgrounds Subtypes'),
			'all_items' => __('All Backgrounds Subtypes'),
			'parent_item' => __('Parent Backgrounds Subtype'),
			'parent_item_colon' => __('Parent Backgrounds Subtype:'),
			'edit_item' => __('Edit Backgrounds Subtype'),
			'update_item' => __('Update Backgrounds Subtype'),
			'add_new_item' => __('Add New Backgrounds Subtype'),
			'new_item_name' => __('New Backgrounds Subtype Name'),
			'menu_name' => __('Backgrounds Subtype'),
		];
		$args = [
			'public' => true,
			'hierarchical' => true, // make it hierarchical (like categories)
			'labels' => $labels,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_quick_edit' => true,
			'query_var' => true
		];
		register_taxonomy('picsart_backgrounds_subtype', ['backgrounds'], $args);
	}
}

add_action('init', 'wporg_register_taxonomy_course');


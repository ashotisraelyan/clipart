<?php
define( 'WPCACHEHOME', '/var/www/clipart/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('WP_CACHE', true); //Added by WP-Cache Manager
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'clipart');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2`I;e)@%yQ6|EH.[u9awQENxG8gYKfJq/!#!uOa_NNCr=P^[L`1E=z3-0GDZ$XZt');
define('SECURE_AUTH_KEY',  ';X8w#-CHCx%Zee9Qa0%h|FA3DEyJ!TRQ3swUP_{R/DGo2s,a%|%E3UT(L^!q}5H|');
define('LOGGED_IN_KEY',    'Kb+~8n%W2waQ6eOytH<}Rq}_/ab~jYDr<q12Iaph~_MrSxZ~)u_!bTqDlCQ}n%K^');
define('NONCE_KEY',        'P|xV&6UbIMaL;78nK5vGR[x8CJ358(q2/F@}~E9NEU+SIn`] uPb{T=z#x28()A9');
define('AUTH_SALT',        '^<tp/_~JQWu8qDY@/H.F% [{2Dr)+Z56YiD8s^&LVCv6D|K2j,/1v:8>q/&sL4Gl');
define('SECURE_AUTH_SALT', '<ZN&KDVGZxOg-t(;$>tR/12vGZEFlTHzf6F@#q^C<o*EC|:@wgy;OXzZZm`JaDJT');
define('LOGGED_IN_SALT',   '8-/ZzGE4ZP|AZ1e7}Si|O)%x9Nr-_yyWd4`arobJ~MoqEhvwu;U L9l@p4 ;73Uf');
define('NONCE_SALT',       'SUwvn;jBeXrQeCIhJZ/D,lsL)EGZ?G:>|o]~`  8A[lDTC=u6vP9GHQ~UxCDUmw-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/* Limit Post Revisions and autosave */
define( 'WP_POST_REVISIONS', 5 );
define('AUTOSAVE_INTERVAL', 180);

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
